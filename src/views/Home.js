/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
  FlatList,
  Image,
  SafeAreaView,
} from 'react-native';
import * as Progress from 'react-native-progress';
import {ImageBackground} from 'react-native';
import Api from '../services/methods';
import HomeSkeleton from '../components/inicio/HomeSkeleton';
import {useIsFocused} from '@react-navigation/native';

const Home = ({navigation}) => {
  const [isLoading, setisLoading] = useState(true);
  const [creatures, setCreatures] = useState([]);
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      Api.get('v1/creatures')
        .then(res => {
          setCreatures(res.data);
          setTimeout(() => {
            setisLoading(false);
          }, 2000);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }, [isFocused]);

  const ItemCreature = ({item}) => {
    return (
      <View style={styles.containItem}>
        <TouchableOpacity
          style={styles.imageTouch}
          onPress={() => {
            navigation.navigate('Creature', {infoCreature: item});
          }}>
          <View>
            <Image
              style={styles.image}
              source={{
                uri: item.avatar,
              }}
            />
            <Text style={styles.itemDescription}>{item.user.username}</Text>
            <Progress.Bar
              progress={item.life / 100}
              width={120}
              color="#F92828"
              borderColor="#F92828"
              style={{marginVertical: 10, alignSelf: 'center'}}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.sectionContainer}>
      <StatusBar backgroundColor="#4847B5" barStyle="dark-content" />
      <ImageBackground
        style={styles.imagefondo}
        source={require('../assets/resources/forest.jpg')}
      />
      {isLoading ? (
        <HomeSkeleton loading={isLoading} />
      ) : (
        <FlatList
          data={creatures}
          keyExtractor={creature => creature.id}
          numColumns={2}
          initialScrollIndex={0}
          style={styles.margen}
          // initialNumToRender={10}
          renderItem={creature => <ItemCreature item={creature.item} />}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    backgroundColor: '#f8f8f8',
  },
  btn: {
    borderRadius: 20,
    backgroundColor: '#4847B5',
    height: 40,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lblBtn: {
    color: '#FFF',
    fontSize: 15,
    fontFamily: 'OpenSans-SemiBold',
  },

  //new
  containItem: {
    marginVertical: 10,
    width: '50%',
    padding: 10,
  },
  imageTouch: {
    height: 150,
    borderRadius: 20,
    justifyContent: 'center',
  },
  image: {
    resizeMode: 'contain',
    width: '100%',
    height: 150,
    // height: normalize(65),
  },
  itemDescription: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: 'OpenSans-SemiBold',
    textTransform: 'uppercase',
    paddingTop: 5,
    color: '#fff',
  },
  imagefondo: {
    flex: 1,
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  infoContain: {
    backgroundColor: '#B5E3C5',
    borderRadius: 10,
    borderColor: '#8AA39B',
    borderWidth: 2,
  },
  margen: {
    marginBottom: 100,
  },
  skeletonContenido: {
    flex: 1,
    width: 300,
  },
});

export default Home;
