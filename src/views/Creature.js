/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {CreatureHeader, CreatureMissions} from '../components';
import Api from '../services/methods';
import AsyncStorage from '@react-native-community/async-storage';

const Creature = ({route}) => {
  const {infoCreature} = route.params;
  const [missions, setmissions] = useState([]);
  const [sub, setSub] = useState([]);
  const [upgrade, setUpgrade] = useState(false);
  let collector = '';

  const listMissions = async () => {
    collector = await AsyncStorage.getItem('user');
    if (infoCreature.lastVersion && infoCreature.lastVersion.id) {
      Api.get(`v1/misions/version/${infoCreature.lastVersion.id}?username=${collector}`)
        .then(res => {
          if (res.data.length > 0) {
            setmissions(res.data);
            listSubMissions();
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  const listSubMissions = async () => {
    Api.get(`v1/submissions/creature/${infoCreature.lastVersion.id}`)
      .then(res => {
        if (res.data.length > 0) {
          setSub(res.data);
        }
      })
      .catch(err => {
        console.log(err.response.data);
      });
  };

  if (upgrade) {
    setUpgrade(false);
    listMissions();
  }

  useEffect(() => {
    listMissions();
  }, []);

  return (
    <View style={styles.sectionContainer}>
      <CreatureHeader infoCreature={infoCreature} />
      <CreatureMissions
        missions={missions}
        infoCreature={infoCreature}
        sub={sub}
        setUpgrade={setUpgrade}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: '2%',
  },
});

export default Creature;
