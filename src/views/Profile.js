import React, {useContext, useEffect, useState} from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import AppContext from '../helpers/context';
import AsyncStorage from '@react-native-community/async-storage';
import {UserData, BtnOption} from '../components';
import ProfileSkeleton from '../components/profile/ProfileSkeleton';
import Api from '../services/methods';
import {useIsFocused} from '@react-navigation/native';

const Profile = ({navigation}) => {
  const [profile, setProfile] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const {signOut} = useContext(AppContext);
  const isFocused = useIsFocused();

  const getDataProfile = async () => {
    setIsLoading(true);
    let user = await AsyncStorage.getItem('user');
    Api.get(`v1/rcollectors/user/${user}`)
      .then(res => {
        setProfile(res.data);
        setIsLoading(false);
      })
      .catch(err => {
        console.log(err);
        Alert.alert('¡Ha ocurrido un error!');
        setIsLoading(false);
      });
  };

  useEffect(() => {
    if (isFocused) {
      getDataProfile();
    }
  }, [isFocused]);

  const Logout = async () => {
    await AsyncStorage.clear();
    setTimeout(() => {
      signOut();
    }, 1000);
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ProfileSkeleton loading={isLoading} />
      ) : (
        <UserData
          points={profile.score}
          stars={profile.stars}
          reports={profile.approvedReports}
          collector={profile.user}
          avatar={profile.avatar}
          misions={0}
        />
      )}
      <View style={styles.btns}>
        <BtnOption
          text={'Editar Perfil'}
          action={() => {
            navigation.navigate('EditProfile');
          }}
        />
        <BtnOption
          text={'Contraseña'}
          action={() => {
            navigation.navigate('EditPassword');
          }}
        />
        <BtnOption
          text={'Recompensas'}
          action={() => {
            navigation.navigate('Rewards', {
              user: profile,
            });
          }}
        />
        <BtnOption
          text={'Cerrar sesión'}
          action={() => {
            Logout();
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
  },
  btns: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
});

export default Profile;
