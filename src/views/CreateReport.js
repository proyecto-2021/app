/* eslint-disable react-native/no-inline-styles */
import React, {useReducer} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Textarea from 'react-native-textarea';
import {createFormData} from '../helpers/formData';
import Api from '../services/methods';
import AsyncStorage from '@react-native-community/async-storage';
import ButtonPrincipal from '../components/detail/ButtonPrincipal';
import ImgInput from '../components/detail/ImgInput';
import alertMessage from '../components/detail/AlertMsg';

const CreateReport = ({navigation, route}) => {
  const {infoCreature, sub} = route.params;
  let collectorId = null;

  const [variables, setVariables] = useReducer(
    (variables, newVariables) => ({...variables, ...newVariables}),
    {
      image: null,
      description: '',
      type: false,
      subMissionId: false,
      animating: false,
    },
  );

  const submit = async () => {
    setVariables({animating: true});
    if (
      variables.description.trim() === '' ||
      variables.type === false ||
      variables.type === 'None'
    ) {
      setVariables({animating: false});
      alertMessage('Error!', 'El tipo de reporte y la descripción son obligatorios');
      return;
    } else {
      collectorId = await AsyncStorage.getItem('id');
      save();
    }
  };

  const updatePhoto = item => {
    setVariables({image: item});
  };

  const save = async () => {
    let item = {
      collectorId: collectorId,
      creatureVersionId: infoCreature.lastVersion.id,
      type: variables.type,
      description: variables.description,
      subMissionId: variables.subMissionId,
    };
    let info = await createFormData(variables.image, item);
    Api.post('v1/reports', info)
      .then(res => {
        console.log(res);
        setVariables({animating: false});
        alertMessage('Proceso Exitoso!', 'Se ha registrado su reporte correctamente');
        navigation.navigate('Creature');
      })
      .catch(err => {
        setVariables({animating: false});
        console.log(err);
        alertMessage('¡Ha ocurrido un error!', 'Por favor, verifique los datos e intente de nuevo');
      });
  };

  const closeKeyboard = () => {
    Keyboard.dismiss();
  };

  return (
    <TouchableWithoutFeedback onPress={() => closeKeyboard()}>
      <ScrollView>
        <View style={styles.sectionContainer}>
          <View>
            <Text style={styles.label}>Tipo de Reporte</Text>
            <View style={styles.pick}>
              <Picker
                mode="dropdown"
                selectedValue={variables.type}
                onValueChange={ty => setVariables({type: ty})}
                style={{marginTop: -8}}>
                <Picker.Item label="- Seleccione -" value="None" />
                <Picker.Item label="Mejora" value="I" />
                <Picker.Item label="Error" value="E" />
              </Picker>
            </View>
          </View>
          <View>
            <Text style={styles.labelDescription}>Descripción</Text>
          </View>
          <View style={styles.containerTextArea}>
            <Textarea
              containerStyle={styles.textArea}
              style={styles.text}
              defaultValue={variables.description}
              onChangeText={desc => setVariables({description: desc})}
              maxLength={150}
              placeholderTextColor={'#4847B5'}
              placeholder={'Ingrese una descripción...'}
            />
          </View>
          <View style={styles.containerMission}>
            <Text style={styles.label}>Submisión Asociada</Text>
            <View style={styles.pick}>
              <Picker
                mode="dropdown"
                selectedValue={variables.subMissionId}
                onValueChange={subMiss => setVariables({subMissionId: subMiss})}
                style={{marginTop: -8}}>
                <Picker.Item label="- Seleccione -" value="None" />
                {sub.map(subMiss => (
                  <Picker.Item key={subMiss.id} label={subMiss.name} value={subMiss.id} />
                ))}
              </Picker>
            </View>
          </View>
          <View>
            <Text style={styles.labelImg}>Imagen</Text>
            <ImgInput img={variables.image} updatePhoto={updatePhoto} />
          </View>
          {variables.animating ? (
            <View style={styles.loading}>
              <ActivityIndicator size="large" color="#0f444c" animating={variables.animating} />
            </View>
          ) : (
            <View style={styles.btnCenter}>
              <ButtonPrincipal action={submit} msg="Guardar" />
            </View>
          )}
        </View>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: '3%',
  },
  labelDescription: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 15,
    marginTop: 1,
    paddingTop: 10,
  },
  containerTextArea: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingBottom: 10,
  },
  textArea: {
    height: 180,
    width: 385,
    paddingVertical: 10,
    backgroundColor: '#f8f8f8',
  },
  text: {
    textAlignVertical: 'top',
    height: 170,
    fontSize: 14,
    borderColor: '#D1D1D1',
    borderWidth: 1,
    borderRadius: 20,
    borderStyle: 'solid',
    paddingHorizontal: 10,
    color: '#000000',
  },
  label: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 15,
  },
  loading: {
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 100,
  },
  containerMission: {
    marginBottom: 30,
  },
  labelImg: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 15,
    marginBottom: 10,
    marginTop: -10,
  },
  pick: {
    borderColor: '#D1D1D1',
    backgroundColor: '#f8f8f8',
    borderRadius: 20,
    borderWidth: 1,
    height: 40,
    marginTop: 10,
  },
  btnCenter: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 60,
  },
});

export default CreateReport;
