/* eslint-disable no-shadow */
import React, {useReducer} from 'react';
import {
  View,
  StyleSheet,
  Text,
  StatusBar,
  SafeAreaView,
  Image,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {RegisterForm} from '../components';
import {Icon} from 'react-native-elements';
import Api from '../services/methods';

const Register = ({navigation}) => {
  const [variables, setVariables] = useReducer(
    (variables, newVariables) => ({...variables, ...newVariables}),
    {
      username: '',
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: '',
      loading: false,
    },
  );

  const registerUser = async () => {
    setVariables({loading: true});
    let item = {
      username: variables.username.trim(),
      firstName: variables.firstName.trim(),
      lastName: variables.lastName.trim(),
      email: variables.email.trim().toLowerCase(),
      password: variables.password.trim(),
    };
    Api.post('v1/auth/register', item)
      .then(res => {
        Alert.alert(`${res.message}`);
        setTimeout(() => {
          setVariables({loading: false});
          navigation.push('Login');
        }, 3000);
      })
      .catch(err => {
        console.log(err);
        Alert.alert('Error al Registrar');
        setVariables({loading: false});
      });
  };

  const closeKeyboard = () => {
    Keyboard.dismiss();
  };

  return (
    <TouchableWithoutFeedback onPress={() => closeKeyboard()}>
      <SafeAreaView style={styles.contain}>
        <StatusBar backgroundColor="#4847B5" barStyle="dark-content" />
        <LinearGradient colors={['#4847B5', '#0F444C']} style={styles.gradient}>
          <View>
            <View style={styles.arrowBack}>
              <Icon
                name="arrow-back-ios"
                color="#fff"
                onPress={() => {
                  navigation.goBack();
                }}
              />
            </View>
            <View style={styles.center}>
              <Image style={styles.image} source={require('../assets/icons/user.png')} />
              <Text style={styles.title}>Registrate!</Text>
            </View>
          </View>
          <RegisterForm
            variables={variables}
            setVariables={setVariables}
            registerUser={registerUser}
          />
          <View style={styles.registerContain}>
            <Text style={styles.lblDontHaveAcc}>
              ¿Ya tienes cuenta?{' '}
              <Text
                style={styles.lblLogin}
                onPress={() => {
                  navigation.push('Login');
                }}>
                Inicia sesión
              </Text>
            </Text>
          </View>
        </LinearGradient>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: '#4847B5',
  },
  gradient: {
    flex: 1,
    paddingHorizontal: '10%',
    backgroundColor: '#4847B5',
  },
  center: {
    alignItems: 'center',
    marginBottom: 20,
    marginTop: -5,
  },
  image: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
  },
  title: {
    fontSize: 20,
    fontFamily: 'OpenSans-Bold',
    color: '#fff',
  },
  arrowBack: {
    marginTop: 20,
    alignItems: 'flex-start',
  },
  registerContain: {
    alignItems: 'center',
    marginVertical: 10,
  },
  lblDontHaveAcc: {
    fontSize: 15,
    color: '#FFF',
    fontFamily: 'OpenSans-Regular',
  },
  lblLogin: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#FFEBFF',
    fontFamily: 'OpenSans-Light',
    textDecorationLine: 'underline',
  },
});

export default Register;
