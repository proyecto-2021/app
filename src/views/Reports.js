import React, {useState, useEffect} from 'react';
import {View, StyleSheet, FlatList, RefreshControl, ToastAndroid, Alert} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import {ReportsList} from '../components';
import Api from '../services/methods';
import ReportsSkeleton from '../components/myReports/ReportsSkeleton';
import withoutData from '../components/detail/AlertWithout';
import AsyncStorage from '@react-native-community/async-storage';

const Reports = ({navigation}) => {
  const [reports, setReports] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const isFocused = useIsFocused();

  const onRefresh = async () => {
    setLoading(true);
    setRefreshing(true);
    try {
      setReports([]);
      Api.get('v1/reports/all')
        .then(res => {
          if (res.data && res.data.length > 0) {
            setReports(res.data);
            setLoading(false);
            ToastAndroid.show('Pantalla Actualizada', ToastAndroid.SHORT);
            setRefreshing(false);
          } else {
            setLoading(false);
            withoutData('No has generado reportes aún', () => navigation.navigate('Home'));
          }
        })
        .catch(err => {
          console.log(err);
          setLoading(false);
          Alert.alert('¡Ha ocurrido un error!');
        });
    } catch (error) {
      console.error(error);
      setLoading(false);
      Alert.alert('¡Ha ocurrido un error!');
    }
  };

  const getReports = async () => {
    setLoading(true);
    const collectorId = await AsyncStorage.getItem('id');
    Api.get(`v1/reports/collector/${collectorId}`)
      .then(res => {
        if (res.data && res.data.length > 0) {
          setReports(res.data);
          setLoading(false);
        } else {
          setLoading(false);
          withoutData('No has generado reportes aún', () => navigation.navigate('Home'));
        }
      })
      .catch(err => {
        console.log(err);
        setLoading(false);
        Alert.alert('¡Ha ocurrido un error!');
      });
  };

  useEffect(() => {
    if (isFocused) {
      getReports();
    }
  }, [isFocused]);

  return (
    <View style={styles.sectionContainer}>
      {loading ? (
        <ReportsSkeleton loading={loading} />
      ) : (
        <FlatList
          data={reports}
          renderItem={({item}) => <ReportsList item={item} />}
          keyExtractor={report => report.id.toString()}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
          style={styles.flat}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: 60,
  },
  flat: {
    marginBottom: 15,
  },
});

export default Reports;
