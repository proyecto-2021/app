import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {NotificationsList} from '../components';
import Api from '../services/methods';
import NotificationsSkeleton from '../components/notifications/NotificationsSkeleton';
import withoutData from '../components/detail/AlertWithout';
import {useIsFocused} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

const Notifications = ({navigation}) => {
  const [notifications, setNotifications] = useState([]);
  const [loading, setLoading] = useState(true);
  const isFocused = useIsFocused();

  const getNotifications = async () => {
    setLoading(true);
    const idUser = await AsyncStorage.getItem('id');
    Api.get(`v1/notifications/collector/${idUser}`)
      .then(res => {
        if (res.data.length > 0) {
          setNotifications(res.data);
          setLoading(false);
        } else {
          setLoading(false);
          withoutData('No has recibido notificaciones aún', () => navigation.navigate('Home'));
        }
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
        Alert.alert('¡Ha ocurrido un error!');
      });
  };

  useEffect(() => {
    if (isFocused) {
      getNotifications();
    }
  }, [isFocused]);

  return (
    <View style={styles.sectionContainer}>
      {loading ? (
        <NotificationsSkeleton loading={loading} />
      ) : (
        <NotificationsList notifications={notifications} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: 60,
  },
});

export default Notifications;
