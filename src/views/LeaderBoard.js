import React, {useState, useEffect} from 'react';
import {View, StyleSheet, FlatList, ActivityIndicator, Alert} from 'react-native';
import {LeaderBoardItem, LeaderBoardHeader} from '../components';
import Api from '../services/methods';

const LeaderBoard = () => {
  const [collectors, setCollectors] = useState([]);
  const [animating, setAnimating] = useState(false);
  const flatListSeparator = () => <View style={styles.separator} />;

  const getCollectors = async () => {
    setAnimating(true);
    Api.get('v1/rcollectors')
      .then(res => {
        res.data.map(item => {
          item.position = res.data.indexOf(item) + 1;
        });
        setCollectors(res.data);
        setAnimating(false);
      })
      .catch(err => {
        console.log(err);
        Alert.alert('¡Ha ocurrido un error!');
        setAnimating(false);
      });
  };

  useEffect(() => {
    getCollectors();
  }, []);

  return (
    <>
      <LeaderBoardHeader />
      {animating ? (
        <View style={styles.loading}>
          <ActivityIndicator size="large" color="#0f444c" animating={animating} />
        </View>
      ) : (
        <View style={styles.container}>
          <FlatList
            data={collectors}
            renderItem={({item}) => <LeaderBoardItem item={item} />}
            keyExtractor={item => item.id.toString()}
            ItemSeparatorComponent={flatListSeparator}
          />
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    marginBottom: 70,
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#8aa39b',
  },
  loading: {
    justifyContent: 'center',
    marginTop: 200,
  },
});

export default LeaderBoard;
