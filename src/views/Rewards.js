import React from 'react';
import {StyleSheet, View, ScrollView, Text} from 'react-native';
import {UserData} from '../components';
import {Avatars} from '../components';
import {Icon} from 'react-native-elements';

const Rewards = ({route}) => {
  const {user} = route.params;

  return (
    <View style={styles.sectionContainer}>
      <UserData
        points={user.score}
        stars={user.stars}
        reports={user.approvedReports}
        collector={user.user}
        avatar={user.avatar}
        misions={0}
      />
      <Text style={styles.title}>Recompensas</Text>
      <View style={styles.info}>
        <Icon name="info" size={20} color="#0f444c" />
        <View style={styles.textContainer}>
          <Text style={styles.text}>Presiona para cambiar el avatar</Text>
        </View>
      </View>
      <ScrollView style={styles.contain}>
        <Avatars stars={user.stars} avatar={user.avatar} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    backgroundColor: '#f8f8f8',
  },
  contain: {
    flex: 1,
  },
  title: {
    fontSize: 18,
    fontFamily: 'OpenSans-Bold',
    textAlign: 'center',
    marginVertical: 10,
  },
  info: {
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  textContainer: {
    paddingTop: 5,
  },
  text: {
    fontSize: 14,
    marginTop: -5,
    marginLeft: 2,
    textAlign: 'left',
    marginVertical: 10,
  },
});

export default Rewards;
