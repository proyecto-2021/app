import React, {useState, useReducer, useEffect} from 'react';
import {EditProfileForm} from '../components';
import {
  View,
  Alert,
  Text,
  StyleSheet,
  Keyboard,
  TouchableWithoutFeedback,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Api from '../services/methods';

const EditProfile = ({navigation}) => {
  const [variables, setVariables] = useReducer(
    (variables, newVariables) => ({...variables, ...newVariables}),
    {
      username: '',
      firstName: '',
      lastName: '',
      email: '',
      user: '',
      loading: false,
    },
  );
  const [animating, setAnimating] = useState(false);

  const getDataProfile = async () => {
    let user = await AsyncStorage.getItem('user');
    Api.get(`v1/users/${user}`)
      .then(res => {
        if (res.data) {
          setVariables({
            username: res.data.username,
            firstName: res.data.firstName,
            lastName: res.data.lastName,
            email: res.data.email,
            user: res.data.username,
          });
          setAnimating(false);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  useEffect(() => {
    setAnimating(true);
    getDataProfile();
  }, []);

  const updateProfile = async () => {
    setVariables({loading: true});
    let item = {
      username: variables.username.trim(),
      firstName: variables.firstName.trim(),
      lastName: variables.lastName.trim(),
      email: variables.email.trim().toLowerCase(),
    };
    Api.put(`v1/users/${variables.user}`, item)
      .then(res => {
        Alert.alert(`${res.message}`);
        setVariables({loading: false});
        setTimeout(() => {
          navigation.navigate('Profile');
        }, 2000);
      })
      .catch(err => {
        console.log(err);
        Alert.alert('Error al Modificar');
        setVariables({loading: false});
      });
  };

  const closeKeyboard = () => {
    Keyboard.dismiss();
  };

  return (
    <TouchableWithoutFeedback onPress={() => closeKeyboard()}>
      <View style={styles.container}>
        <Text style={styles.title}>Recolector</Text>
        {animating ? (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color="#0f444c" animating={animating} />
          </View>
        ) : (
          <EditProfileForm
            variables={variables}
            setVariables={setVariables}
            updateProfile={updateProfile}
          />
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    alignItems: 'center',
  },
  title: {
    color: '#4847B5',
    fontSize: 20,
    fontFamily: 'OpenSans-Bold',
    marginVertical: 20,
  },
  loading: {
    justifyContent: 'center',
    marginTop: 200,
  },
});

export default EditProfile;
