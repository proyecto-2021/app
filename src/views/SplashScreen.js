import React from 'react';
import {View, StyleSheet, StatusBar, Image} from 'react-native';

const SplashScreen = () => {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#EEF0FA" barStyle="dark-content" />
      <Image style={styles.image} source={require('../assets/gif/Splash.gif')} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#EEF0FA',
  },
  image: {
    resizeMode: 'contain',
    width: '100%',
    // height: normalize(250),
  },
});

export default SplashScreen;
