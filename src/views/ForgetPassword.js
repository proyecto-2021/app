import React, {useState} from 'react';
import {View, StyleSheet, Image, StatusBar, SafeAreaView, Alert} from 'react-native';
import {FormForgetPassword, CheckForgetPassword} from '../components';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'react-native-elements';
import Api from '../services/methods';

const ForgetPassword = ({navigation}) => {
  const [forgetCheck, setForgetCheck] = useState(false);
  const [loading, setLoading] = useState(false);

  const fogetPassword = async item => {
    setLoading(true);
    Api.post('v1/auth/restartPassword', item)
      .then(res => {
        Alert.alert(`${res.message}`);
        setTimeout(() => {
          setLoading(false);
          setForgetCheck(true);
        }, 3000);
      })
      .catch(err => {
        console.log(err);
        Alert.alert('Error al resetear contraseña');
        setLoading(false);
      });
  };
  return (
    <SafeAreaView style={styles.contain}>
      <StatusBar backgroundColor="#4847B5" barStyle="dark-content" />
      <LinearGradient colors={['#4847B5', '#0F444C']} style={styles.gradient}>
        <View style={styles.arrowBack}>
          <Icon
            name="arrow-back-ios"
            color="#fff"
            onPress={() => {
              navigation.goBack();
            }}
          />
        </View>
        <View style={styles.center}>
          <Image style={styles.image} source={require('../assets/icons/lock.png')} />
        </View>
        {forgetCheck ? (
          <CheckForgetPassword navigation={navigation} />
        ) : (
          <FormForgetPassword loading={loading} fogetPassword={fogetPassword} />
        )}
      </LinearGradient>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
  },
  gradient: {
    flex: 1,
    paddingHorizontal: '10%',
    backgroundColor: '#4847B5',
  },
  center: {
    alignItems: 'center',
  },
  image: {
    resizeMode: 'contain',
    width: 120,
    height: 120,
  },
  arrowBack: {
    marginVertical: 50,
    alignItems: 'flex-start',
  },
});

export default ForgetPassword;
