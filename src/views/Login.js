/* eslint-disable no-shadow */
import React, {useReducer, useContext} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  StatusBar,
  SafeAreaView,
  Alert,
  ScrollView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AppContext from '../helpers/context';
import AsyncStorage from '@react-native-community/async-storage';

import Api from '../services/methods';

const Login = ({navigation}) => {
  const {signIn} = useContext(AppContext);
  const [variables, setVariables] = useReducer(
    (variables, newVariables) => ({...variables, ...newVariables}),
    {
      email: '',
      password: '',
      loading: false,
    },
  );

  const handleClick = () => {
    if (variables.email && variables.password) {
      setVariables({loading: true});
      let item = {
        username: variables.email.trim(),
        password: variables.password.trim(),
      };
      Api.post('v1/auth/login', item)
        .then(res => {
          if (res.message === 'OK') {
            if (res.data.type && res.data.type === 'CO') {
              AsyncStorage.setItem('type', res.data.type);
              AsyncStorage.setItem('user', res.data.username);
              AsyncStorage.setItem('id', JSON.stringify(res.data.id));
              AsyncStorage.setItem('token', res.data.token);
              setTimeout(() => {
                setVariables({loading: false});
                signIn();
              }, 2000);
            } else {
              setVariables({loading: false});
              Alert.alert('Acceso restringido', 'El usuario no tiene permiso para ingresar.');
            }
          } else {
            Alert.alert(res.message);
          }
        })
        .catch(err => {
          console.log(err);
          Alert.alert('Error al Ingresar');
          setVariables({loading: false});
        });
    } else {
      if (!variables.email) {
        Alert.alert('Error! Ingrese nombre de usuario');
      }
      if (!variables.password) {
        Alert.alert('Error! Ingrese Contraseña');
      }
    }
  };

  return (
    <SafeAreaView style={styles.contain}>
      <StatusBar backgroundColor="#4847B5" barStyle="dark-content" />
      <LinearGradient colors={['#4847B5', '#0F444C']} style={styles.gradient}>
        <ScrollView>
          <View style={styles.center}>
            <Image style={styles.image} source={require('../assets/resources/logoPng.png')} />
          </View>
          <View style={styles.welcomeContain}>
            <Text style={styles.lblWelcome}>Bienvenido!</Text>
          </View>
          <View style={styles.input}>
            <TextInput
              style={styles.textPlaceholder}
              value={variables.email}
              onChangeText={val => setVariables({email: val})}
              placeholder="Nombre de Usuario"
              placeholderTextColor="#4847B5"
            />
          </View>
          <View style={styles.input}>
            <TextInput
              secureTextEntry
              style={styles.textPlaceholder}
              value={variables.password}
              onChangeText={val => setVariables({password: val})}
              placeholder="Contraseña"
              placeholderTextColor="#4847B5"
            />
          </View>

          <View style={styles.center}>
            <Text
              style={styles.lblForget}
              onPress={() => {
                navigation.navigate('ForgetPassword');
              }}>
              ¿Olvidaste tu contraseña?
            </Text>
          </View>

          <View style={styles.center}>
            {!variables.loading ? (
              <TouchableOpacity
                style={styles.btnLog}
                onPress={() => {
                  handleClick();
                }}>
                <Text style={styles.lblBtn}>Iniciar sesión</Text>
              </TouchableOpacity>
            ) : (
              <View style={styles.loading}>
                <ActivityIndicator size="large" color="#FFFFFF" animating={variables.loading} />
              </View>
            )}
          </View>

          <View style={styles.registerContain}>
            <Text style={styles.lblDontHaveAcc}>
              ¿No tienes cuenta? {''}
              <Text
                style={styles.lblRegister}
                onPress={() => {
                  navigation.push('Register');
                }}>
                Registrate aquí
              </Text>
            </Text>
          </View>
        </ScrollView>
      </LinearGradient>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
  },
  gradient: {
    flex: 1,
    paddingHorizontal: '10%',
    backgroundColor: '#4847B5',
  },
  center: {
    alignItems: 'center',
  },
  image: {
    resizeMode: 'contain',
    width: 280,
    height: 280,
  },
  welcomeContain: {
    alignItems: 'center',
    paddingBottom: 20,
    marginTop: -70,
  },
  lblWelcome: {
    color: '#FFF',
    fontSize: 22,
    fontFamily: 'OpenSans-Bold',
    paddingTop: 40,
  },
  input: {
    flexDirection: 'row',
    height: 55,
    paddingHorizontal: 10,
    color: '#FFF',
    textDecorationLine: 'none',
    marginBottom: 15,
    borderRadius: 30,
    backgroundColor: '#FFF',
    borderColor: '#4847B5',
    borderWidth: 3,
  },
  textPlaceholder: {
    fontSize: 16,
    fontFamily: 'OpenSans-Regular',
    flex: 1,
    color: '#000000',
  },
  lblForget: {
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
    color: '#FFF',
    textDecorationLine: 'underline',
  },
  btnLog: {
    borderColor: '#fff',
    borderWidth: 0.3,
    borderRadius: 20,
    backgroundColor: '#4847B5',
    height: 45,
    width: '100%',
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lblBtn: {
    color: '#FFF',
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
    marginVertical: 10,
  },
  lblDontHaveAcc: {
    fontSize: 15,
    color: '#FFF',
    fontFamily: 'OpenSans-Regular',
  },
  lblRegister: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#FFEBFF',
    fontFamily: 'OpenSans-Light',
    textDecorationLine: 'underline',
  },
  loading: {
    justifyContent: 'center',
    marginTop: 10,
  },
  registerContain: {
    alignItems: 'center',
    marginTop: 15,
  },
});

export default Login;
