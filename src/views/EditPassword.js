import React, {useState} from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {FormEditPassword} from '../components';
import Api from '../services/methods';
import {useNavigation} from '@react-navigation/native';

const EditPassword = () => {
  const [animating, setAnimating] = useState(false);
  const navigation = useNavigation();

  const updatePassword = async item => {
    setAnimating(true);
    Api.patch('v1/users/updateMyPassword', item)
      .then(res => {
        Alert.alert(`${res.message}`);
        setAnimating(false);
        navigation.navigate('Profile');
      })
      .catch(err => {
        if (err.response.data.error && err.response.data.error === 'Contraseña actual incorrecta') {
          Alert.alert(`${err.response.data.error}`);
          setAnimating(false);
        } else {
          console.log(err);
          Alert.alert('Error al actualizar contraseña');
          setAnimating(false);
          navigation.navigate('Profile');
        }
      });
  };
  return (
    <View style={styles.container}>
      <FormEditPassword animating={animating} updatePassword={updatePassword} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    alignItems: 'center',
  },
});

export default EditPassword;
