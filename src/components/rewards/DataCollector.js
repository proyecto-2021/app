import React from 'react';
import {StyleSheet, View, Image, Text} from 'react-native';

const DataCollector = () => {
  return (
    <View style={styles.sectionContainer}>
      <Image
        style={styles.image}
        source={require('../../assets/resources/Actores/Avatares/Fraile.png')}
      />
      <View>
        <Text style={styles.username}>danielacadenasm</Text>
        <Text style={styles.starts}>2320 estrellas</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flexDirection: 'row',
    backgroundColor: '#4847B5',
    padding: '3%',
    alignItems: 'center',
    borderRadius: 10,
    width: '100%',
  },
  image: {
    width: 100,
    height: 100,
  },
  username: {
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'OpenSans-Bold',
    color: '#fff',
  },
  starts: {
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'OpenSans',
    color: '#fff',
  },
});

export default DataCollector;
