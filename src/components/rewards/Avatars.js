import React, {useState} from 'react';
import {View, StyleSheet, ActivityIndicator, Alert} from 'react-native';
import Api from '../../services/methods';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';
import AvatarsComponent from './AvatarsComponent';

const Avatars = ({stars, avatar}) => {
  const [animating, setAnimating] = useState(false);
  const navigation = useNavigation();
  let collectorId = '';

  const changeAvatar = async valor => {
    if (valor !== avatar) {
      setAnimating(true);
      collectorId = await AsyncStorage.getItem('id');
      Api.put(`v1/rcollectors/avatar/${collectorId}`, {avatar: valor})
        .then(res => {
          console.log(res);
          setAnimating(false);
          navigation.push('Profile');
        })
        .catch(err => {
          console.log(err.response);
          Alert.alert('¡Ha ocurrido un error!');
          setAnimating(false);
        });
    }
  };

  return (
    <>
      {animating ? (
        <View style={styles.loading}>
          <ActivityIndicator size="large" color="#0f444c" animating={animating} />
        </View>
      ) : (
        <AvatarsComponent stars={stars} changeAvatar={changeAvatar} />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  loading: {
    justifyContent: 'center',
    marginTop: 100,
  },
});

export default Avatars;
