import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity, Icon} from 'react-native';

const Avatars = ({stars, changeAvatar}) => {
  return (
    <>
      <View style={styles.container}>
        <View style={styles.items}>
          <TouchableOpacity
            style={styles.btnAvatar}
            onPress={() => changeAvatar('https://i.ibb.co/jJVj3Yj/Fraile.png')}>
            <Image
              style={styles.imgAvatar}
              source={require('../../assets/resources/Actores/Avatares/Fraile.png')}
            />
            <Text style={styles.txtTitle}>Fraile</Text>
            <Text style={styles.txtStars}>0 Estrellas</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.items}>
          <TouchableOpacity
            style={styles.btnAvatar}
            disabled={stars >= 10 ? false : true}
            onPress={() => changeAvatar('https://i.ibb.co/MRczJ83/Espadachin.png')}>
            {stars >= 10 ? null : (
              <View>
                <Image
                  style={styles.imgBlock}
                  source={require('../../assets/resources/candado.png')}
                />
              </View>
            )}
            <Image
              style={styles.imgAvatar}
              source={require('../../assets/resources/Actores/Avatares/Espadachin.png')}
            />
            <Text style={styles.txtTitle}>Espadachin</Text>
            <Text style={styles.txtStars}>10 Estrellas</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.container}>
        <View style={styles.items}>
          <TouchableOpacity
            style={styles.btnAvatar}
            disabled={stars >= 25 ? false : true}
            onPress={() => changeAvatar('https://i.ibb.co/ykkQwHS/Arquero.png')}>
            {stars >= 25 ? null : (
              <View>
                <Image
                  style={styles.imgBlock}
                  source={require('../../assets/resources/candado.png')}
                />
              </View>
            )}
            <Image
              style={styles.imgAvatar}
              source={require('../../assets/resources/Actores/Avatares/Arquero.png')}
            />
            <Text style={styles.txtTitle}>Arquero</Text>
            <Text style={styles.txtStars}>25 Estrellas</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.items}>
          <TouchableOpacity
            style={styles.btnAvatar}
            disabled={stars >= 50 ? false : true}
            onPress={() => changeAvatar('https://i.ibb.co/DGGQScD/Caballero.png')}>
            {stars >= 50 ? null : (
              <View>
                <Image
                  style={styles.imgBlock}
                  source={require('../../assets/resources/candado.png')}
                />
              </View>
            )}
            <Image
              style={styles.imgAvatar}
              source={require('../../assets/resources/Actores/Avatares/Caballero.png')}
            />
            <Text style={styles.txtTitle}>Caballero</Text>
            <Text style={styles.txtStars}>50 Estrellas</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.container}>
        <View style={styles.items}>
          <TouchableOpacity
            style={styles.btnAvatar}
            disabled={stars >= 75 ? false : true}
            onPress={() => changeAvatar('https://i.ibb.co/gw0BjBF/Guerrero-Varega.png')}>
            {stars >= 75 ? null : (
              <View>
                <Image
                  style={styles.imgBlock}
                  source={require('../../assets/resources/candado.png')}
                />
              </View>
            )}
            <Image
              style={styles.imgAvatar}
              source={require('../../assets/resources/Actores/Avatares/Guerrero-Varega.png')}
            />
            <Text style={styles.txtTitle}>Guerrero Varega</Text>
            <Text style={styles.txtStars}>75 Estrellas</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.items}>
          <TouchableOpacity
            style={styles.btnAvatar}
            disabled={stars >= 100 ? false : true}
            onPress={() => changeAvatar('https://i.ibb.co/Wk6XHvN/Guardian-Medio.png')}>
            {stars >= 100 ? null : (
              <View>
                <Image
                  style={styles.imgBlock}
                  source={require('../../assets/resources/candado.png')}
                />
              </View>
            )}
            <Image
              style={styles.imgAvatar}
              source={require('../../assets/resources/Actores/Avatares/Guardian-Medio.png')}
            />
            <Text style={styles.txtTitle}>Guardian Medio</Text>
            <Text style={styles.txtStars}>100 Estrellas</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.containerLast}>
        <View style={styles.items}>
          <TouchableOpacity
            style={styles.btnAvatar}
            disabled={stars >= 250 ? false : true}
            onPress={() => changeAvatar('https://i.ibb.co/9rzgW9G/Guardian-Oscuro.png')}>
            {stars >= 250 ? null : (
              <View>
                <Image
                  style={styles.imgBlock}
                  source={require('../../assets/resources/candado.png')}
                />
              </View>
            )}
            <Image
              style={styles.imgAvatar}
              source={require('../../assets/resources/Actores/Avatares/Guardian-Oscuro.png')}
            />
            <Text style={styles.txtTitle}>Guardian Oscuro</Text>
            <Text style={styles.txtStars}>250 Estrellas</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.items}>
          <TouchableOpacity
            style={styles.btnAvatar}
            disabled={stars >= 500 ? false : true}
            onPress={() => changeAvatar('https://i.ibb.co/DVGrVKs/Rey.png')}>
            {stars >= 500 ? null : (
              <View>
                <Image
                  style={styles.imgBlock}
                  source={require('../../assets/resources/candado.png')}
                />
              </View>
            )}
            <Image
              style={styles.imgAvatar}
              source={require('../../assets/resources/Actores/Avatares/Rey.png')}
            />
            <Text style={styles.txtTitle}>Rey</Text>
            <Text style={styles.txtStars}>500 Estrellas</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  containerLast: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 100,
  },
  items: {
    flexBasis: '40%',
    marginHorizontal: 5,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#4847B5',
    borderRadius: 20,
    padding: 10,
  },
  imgAvatar: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 100,
  },
  imgBlock: {
    position: 'absolute',
    zIndex: 1000,
    width: 18,
    height: 25,
    left: 55,
    top: -5,
    paddingTop: 10,
  },
  txtTitle: {
    fontSize: 15,
    fontFamily: 'OpenSans-Bold',
  },
  txtStars: {
    fontSize: 13,
    fontFamily: 'OpenSans-SemiBold',
  },
  btnAvatar: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Avatars;
