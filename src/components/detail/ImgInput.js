import React from 'react';
import {View, Text, StyleSheet, TouchableHighlight, TouchableOpacity, Image} from 'react-native';
import * as ImagePicker from 'react-native-image-picker';
import {Icon} from 'react-native-elements';

const ImgInput = ({img, updatePhoto}) => {
  const handleChoosePhoto = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, response => {
      if (response.assets && response.assets[0].uri) {
        updatePhoto(response.assets[0]);
      }
    });
  };

  return (
    <>
      {img && img.uri ? (
        <View>
          <View style={styles.containerImage}>
            <TouchableOpacity
              style={styles.closeBtn}
              onPress={() => {
                updatePhoto(null);
              }}
              underlayColor="#ffff">
              <Icon name="close" size={25} color="white" />
            </TouchableOpacity>
            <Image
              style={styles.imagenPick}
              source={{
                uri: img.uri,
              }}
            />
          </View>
        </View>
      ) : (
        <TouchableHighlight
          style={styles.inputContainer}
          onPress={() => {
            handleChoosePhoto();
          }}
          underlayColor="#ffff">
          <Text style={styles.labelSelect}>Buscar Imagen</Text>
        </TouchableHighlight>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  containerImage: {
    alignItems: 'center',
    position: 'relative',
  },
  closeBtn: {
    backgroundColor: 'red',
    borderRadius: 100,
    padding: 5,
    position: 'absolute',
    zIndex: 1000,
    right: 0,
    marginRight: 10,
    marginTop: 10,
  },
  imagenPick: {
    width: '100%',
    height: 370,
    resizeMode: 'stretch',
    borderRadius: 20,
    marginBottom: 10,
  },
  inputContainer: {
    marginBottom: 15,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    borderColor: '#D1D1D1',
    borderWidth: 1,
  },
  labelSelect: {
    fontSize: 16,
    marginLeft: 5,
    color: '#4847B5',
  },
});

export default ImgInput;
