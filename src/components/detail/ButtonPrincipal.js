import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';

const ButtonPrincipal = ({action, msg}) => {
  return (
    <TouchableOpacity onPress={action} style={styles.btn}>
      <Text style={styles.text}> {msg} </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    padding: 10,
    backgroundColor: '#4847B5',
    marginVertical: 25,
    width: 130,
    borderRadius: 20,
  },
  text: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 15,
    textAlign: 'center',
    color: '#ffffff',
  },
});

export default ButtonPrincipal;
