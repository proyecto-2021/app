import {Alert} from 'react-native';

const alertMessage = (msg1, msg2) => {
  return Alert.alert(msg1, msg2);
};

export default alertMessage;
