import {Alert} from 'react-native';

const withoutData = (msg, action) => {
  return Alert.alert('¡Alerta!', msg, [{text: 'OK', onPress: action}]);
};

export default withoutData;
