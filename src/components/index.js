// Index de importacion/exportacin para componentes
import LeaderBoardItem from './leaderBoard/LeaderBoardItem';
import LeaderBoardHeader from './leaderBoard/LeaderBoardHeader';
import DataCollector from './rewards/DataCollector';
import Avatars from './rewards/Avatars';
import Notification from './notifications/Notification';
import NotificationsList from './notifications/NotificationsList';
import CheckForgetPassword from './forgetPassword/CheckForgetPassword';
import FormForgetPassword from './forgetPassword/FormForgetPassword';
import ReportDetail from './myReports/ReportDetail';
import ReportsList from './myReports/ReportsList';
import CreatureHeader from './creatures/CreatureHeader';
import CreatureMissions from './creatures/CreatureMissions';
import FormEditPassword from './editPassword/FormEditPassword';
import UserData from './profile/UserData';
import BtnOption from './profile/BtnOption';
import EditProfileForm from './editProfile/EditProfileForm';
import RegisterForm from './register/RegisterForm';
import HomeSkeleton from './inicio/HomeSkeleton';

export {
  LeaderBoardItem,
  LeaderBoardHeader,
  CheckForgetPassword,
  FormForgetPassword,
  ReportDetail,
  ReportsList,
  CreatureHeader,
  CreatureMissions,
  DataCollector,
  Avatars,
  Notification,
  NotificationsList,
  FormEditPassword,
  UserData,
  BtnOption,
  EditProfileForm,
  RegisterForm,
  HomeSkeleton,
};
