import React from 'react';
import {StyleSheet} from 'react-native';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const CreatureMissionsSkeleton = ({loading}) => {
  return (
    <SkeletonContent
      containerStyle={styles.skeletonArea}
      isLoading={loading}
      animationDirection="horizontalRight"
      layout={[
        {width: 395, height: 55, marginBottom: 7, borderRadius: 20},
        {width: 395, height: 55, marginBottom: 7, borderRadius: 20},
        {width: 395, height: 55, marginBottom: 7, borderRadius: 20},
        {width: 395, height: 55, marginBottom: 7, borderRadius: 20},
        {width: 395, height: 55, marginBottom: 7, borderRadius: 20},
        {
          width: 130,
          height: 40,
          marginTop: 45,
          marginBottom: 7,
          borderRadius: 20,
          marginLeft: 265,
        },
      ]}
    />
  );
};

const styles = StyleSheet.create({
  skeletonArea: {
    flex: 1,
    width: 300,
  },
});

export default CreatureMissionsSkeleton;
