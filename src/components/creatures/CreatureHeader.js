/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity, Linking} from 'react-native';
import * as Progress from 'react-native-progress';
import alertMessage from '../detail/AlertMsg';
const CreatureHeader = ({infoCreature}) => {
  const goToUrl = () => {
    if (infoCreature.lastVersion.url && infoCreature.lastVersion && infoCreature) {
      Linking.openURL(infoCreature.lastVersion.url);
      console.log(infoCreature.lastVersion.url);
    } else {
      alertMessage('Atención!!', 'URL no disponible');
    }
  };

  return (
    <>
      <View style={styles.containerHeader}>
        <View style={styles.containerLeft}>
          <View style={styles.containerLeftCreature}>
            <Image
              source={{
                uri: infoCreature.avatar,
              }}
              style={styles.imgCreature}
            />
          </View>
        </View>
        <View style={styles.containerRight}>
          <View style={styles.containerRightHealth}>
            <View style={styles.containerRightHealthImg}>
              <Image source={require('../../assets/resources/vida.png')} style={styles.imgHealth} />
            </View>
            <View style={styles.containerRightHealthBar}>
              <View>
                <Progress.Bar
                  progress={infoCreature.life / 100}
                  width={245}
                  color="#F92828"
                  borderColor="#0f444c"
                  style={{marginTop: 9}}
                />
                <View style={styles.containerRightHealthText}>
                  <Text style={styles.textHealthNumber}> {infoCreature.life}/100</Text>
                  <Text style={styles.textHealth}> Salud </Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.containerSpecification}>
            <Text style={styles.textNameDesc}>
              Nombre:
              <Text style={{fontWeight: 'normal'}}> {infoCreature.description}</Text>
            </Text>
            <Text style={styles.textNameDesc}>
              Descripción:
              <Text style={{fontWeight: 'normal'}}> {infoCreature.description}</Text>
            </Text>
            <TouchableOpacity
              onPress={() => {
                goToUrl();
              }}
              style={styles.btn}>
              <Text style={styles.text}> Url </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{marginLeft: 5}}>
        {infoCreature.lastVersion && infoCreature.lastVersion.id ? (
          <Text style={{fontFamily: 'OpenSans-Regular', fontSize: 13}}>
            Versión: {infoCreature.lastVersion.name}
          </Text>
        ) : (
          <Text>Versión: No tiene</Text>
        )}
      </View>
      <View style={styles.containerTittle}>
        <Text style={styles.tittle}>Listado de Misiones</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerHeader: {
    flexDirection: 'row',
  },
  containerLeft: {
    flex: 2.4,
    padding: 1,
  },
  containerLeftCreature: {
    backgroundColor: '#FFFFFF',
    width: 108,
    borderRadius: 10,
    borderColor: '#0f444c',
    borderStyle: 'solid',
    borderWidth: 1,
  },
  imgCreature: {
    width: 80,
    height: 80,
    marginTop: 10,
    margin: 15,
  },
  containerRight: {
    flex: 6,
  },
  containerRightHealth: {
    flexDirection: 'row',
  },
  containerRightHealthImg: {
    flex: 0.6,
  },
  imgHealth: {
    width: 20,
    height: 20,
    marginTop: 8,
    marginLeft: 5,
  },
  containerRightHealthBar: {
    flex: 5,
  },
  containerRightHealthText: {
    textAlignVertical: 'top',
    flexDirection: 'row',
  },
  containerSpecification: {
    marginHorizontal: 5,
    marginTop: 5,
  },
  textNameDesc: {
    fontWeight: 'bold',
    fontSize: 12,
  },
  textHealthNumber: {
    fontSize: 12,
    flex: 8,
  },
  textHealth: {
    fontSize: 12,
    flex: 1.5,
    fontFamily: 'OpenSans-Regular',
  },
  containerTittle: {
    marginTop: 25,
    marginBottom: 10,
  },
  tittle: {
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
  },
  btn: {
    backgroundColor: '#4847B5',
    width: 90,
    borderRadius: 20,
    marginTop: 5,
  },
  text: {
    color: '#FFFFFF',
    fontFamily: 'OpenSans-Bold',
    textAlign: 'center',
    fontSize: 15,
  },
});

export default CreatureHeader;
