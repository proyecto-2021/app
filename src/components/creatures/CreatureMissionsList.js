import React, {useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Modal} from 'react-native';
import {Icon} from 'react-native-elements';
import CreatureModalSubMission from './CreatureModalSubMission';

const CreatureMissionsList = ({mission, setUpgrade}) => {
  const [modalMission, setModalMission] = useState(false);

  const showModalMission = () => {
    setModalMission(true);
  };

  const lineText = {
    textDecorationLine: 'line-through',
  };

  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity onPress={showModalMission}>
          <View style={styles.containerItems}>
            <View style={styles.containerLeft}>
              <View style={styles.description}>
                {mission.completed === true ? (
                  <Text style={[styles.text, lineText]}> {mission.description} </Text>
                ) : (
                  <Text style={[styles.text]}> {mission.description} </Text>
                )}
              </View>
              <View style={styles.score}>
                <Text style={styles.textScore}> Puntuación: {mission.score} </Text>
              </View>
            </View>
            <View style={styles.containerRight}>
              {mission.completed === true ? (
                <Icon name="check" size={20} color="#0f444c" />
              ) : (
                <Icon name="list" size={20} color="#4847B5" />
              )}
            </View>
          </View>
          <Modal transparent={true} visible={modalMission}>
            <CreatureModalSubMission
              mission={mission}
              setModalMission={setModalMission}
              setUpgrade={setUpgrade}
            />
          </Modal>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    padding: 8,
    borderRadius: 20,
    borderStyle: 'solid',
    borderColor: '#0f444c',
    borderWidth: 1,
    marginBottom: 8,
  },
  containerItems: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerLeft: {
    flex: 6,
    marginBottom: 2,
  },
  description: {
    alignItems: 'flex-start',
  },
  text: {
    color: '#000000',
    fontFamily: 'OpenSans-SemiBold',
  },
  score: {
    marginTop: 2,
  },
  textScore: {
    fontFamily: 'OpenSans-Italic',
    fontSize: 12,
    textAlign: 'left',
    paddingTop: 10,
  },
  containerRight: {
    padding: 1,
    flex: 0.4,
    alignItems: 'flex-end',
  },
});

export default CreatureMissionsList;
