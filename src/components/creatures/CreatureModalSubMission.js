/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, StyleSheet, TouchableOpacity, ActivityIndicator} from 'react-native';
import {Icon} from 'react-native-elements';
import CreatureSubMissionDetail from './CreatureSubMissionDetail';
import Api from '../../services/methods';

const CreatureModalSubMission = ({mission, setModalMission, setUpgrade}) => {
  const [subMissions, setSubMissions] = useState([]);
  const [animating, setAnimating] = useState(false);
  let array = [];

  const hideModal = () => {
    setUpgrade(true);
    setModalMission(false);
  };

  useEffect(() => {
    setAnimating(true);
    Api.get(`v1/submissions/mission/${mission.id}`)
      .then(res => {
        if (res.data.length > 0) {
          res.data.map(sub => {
            if (sub.completed === false) {
              array.push(sub);
              setSubMissions(array);
            }
          });
        }
        setAnimating(false);
      })
      .catch(err => {
        setAnimating(false);
        console.log(err);
      });
  }, []);

  return (
    <>
      <View style={styles.modalPrincipal}>
        <View style={styles.modalSecondary}>
          <View style={styles.header}>
            <TouchableOpacity onPress={hideModal}>
              <Icon name="arrow-back" size={25} color="#0f444c" />
            </TouchableOpacity>
            <View style={{marginTop: 3}}>
              <Text style={{fontSize: 15, fontFamily: 'OpenSans-SemiBold'}}>
                Listado de Submisiones
              </Text>
            </View>
          </View>
          <View style={styles.containerSub}>
            <Text style={styles.tittleSub}> {mission.name} </Text>
          </View>
          {animating ? (
            <View style={styles.loading}>
              <ActivityIndicator size="large" color="#0f444c" animating={animating} />
            </View>
          ) : (
            <>
              {subMissions.length > 0 ? (
                <>
                  <FlatList
                    data={subMissions}
                    renderItem={({item}) => <CreatureSubMissionDetail sub={item} />}
                    keyExtractor={sub => sub.id.toString()}
                    style={{flex: 1}}
                  />
                </>
              ) : (
                <View style={styles.containerTittle}>
                  <Text style={styles.tittle}>No hay submisiones</Text>
                </View>
              )}
            </>
          )}
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  modalPrincipal: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  modalSecondary: {
    backgroundColor: '#f8f8f8',
    margin: 20,
    paddingTop: 10,
    borderRadius: 20,
    flex: 1,
    paddingHorizontal: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  back: {
    alignItems: 'flex-start',
  },
  loading: {
    justifyContent: 'center',
    marginTop: 200,
  },
  containerTittle: {
    marginTop: 200,
  },
  tittle: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#4847B5',
  },
  containerSub: {
    marginTop: 25,
    marginBottom: 20,
  },
  tittleSub: {
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'OpenSans-Bold',
    color: '#000000',
  },
});

export default CreatureModalSubMission;
