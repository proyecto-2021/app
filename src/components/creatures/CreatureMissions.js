import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import CreatureMissionsList from './CreatureMissionsList';
import CreatureMissionsSkeleton from './CreatureMissionsSkeleton';
import {useNavigation} from '@react-navigation/native';

const CreatureMissions = ({missions, infoCreature, sub, setUpgrade}) => {
  const navigation = useNavigation();
  const [validate, setValidate] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setValidate(false);
    if (missions.length > 0) {
      setLoading(false);
      setValidate(true);
    } else {
      setLoading(false);
      setValidate(false);
    }
  }, [missions]);
  return (
    <>
      {loading ? (
        <CreatureMissionsSkeleton loading={loading} />
      ) : (
        <>
          {validate ? (
            <>
              <FlatList
                data={missions}
                renderItem={({item}) => (
                  <CreatureMissionsList mission={item} setUpgrade={setUpgrade} />
                )}
                keyExtractor={mission => mission.id.toString()}
                style={styles.flat}
              />
              <View style={styles.containerButtom}>
                <TouchableOpacity
                  style={styles.btn}
                  onPress={() => {
                    navigation.navigate('CreateReport', {
                      infoCreature: infoCreature,
                      sub: sub,
                    });
                  }}>
                  <Text style={styles.textBtn}>Crear Reporte</Text>
                </TouchableOpacity>
              </View>
            </>
          ) : (
            <View style={styles.containerTittle}>
              <Text style={styles.tittle}>No hay misiones disponibles</Text>
            </View>
          )}
        </>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  flat: {
    flex: 1,
  },
  containerButtom: {
    alignItems: 'flex-end',
    height: 120,
  },
  btn: {
    borderRadius: 20,
    backgroundColor: '#4847B5',
    height: 40,
    width: 130,
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtn: {
    color: '#FFF',
    fontSize: 15,
    fontFamily: 'OpenSans-SemiBold',
  },
  containerTittle: {
    marginTop: 25,
    marginBottom: 10,
  },
  tittle: {
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  loading: {
    justifyContent: 'center',
    marginTop: 30,
  },
});

export default CreatureMissions;
