import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import Api from '../../services/methods';
import AsyncStorage from '@react-native-community/async-storage';

const CreatureSubMissionDetail = ({sub}) => {
  const [check, setCheck] = useState(sub.completed);
  let collectorId = '';

  const valorCheck = async valor => {
    setCheck(valor);
    collectorId = await AsyncStorage.getItem('id');
    Api.post('v1/missionCollectors/', {
      subMissionId: sub.id,
      collectorId: collectorId,
    })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err.response);
      });
  };

  return (
    <>
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={{flex: 0.9}}>
            <Text style={styles.textDescription}> {sub.description} </Text>
          </View>
          <View style={styles.containerCheck}>
            <CheckBox
              tintColors={{true: '#0f444c', false: 'gray'}}
              value={check}
              onValueChange={valor => valorCheck(valor)}
            />
          </View>
        </View>
        <View style={styles.score}>
          <Text style={styles.textScore}> Puntuación: {sub.score} </Text>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 5,
    paddingLeft: 8,
    paddingVertical: 5,
    borderColor: '#0f444c',
    borderWidth: 1,
    borderRadius: 20,
    marginBottom: 8,
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
  },
  textDescription: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 17,
    paddingTop: 2,
  },
  containerCheck: {
    padding: 1,
    marginTop: 8,
    flex: 0.1,
  },
  score: {
    marginTop: -17,
    marginBottom: 3,
    paddingLeft: 2,
  },
  textScore: {
    fontFamily: 'OpenSans-Italic',
    fontSize: 13,
    paddingTop: 15,
  },
});

export default CreatureSubMissionDetail;
