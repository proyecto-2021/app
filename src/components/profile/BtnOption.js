/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';
const BtnOption = ({action, text}) => {
  return (
    <View>
      <TouchableOpacity style={styles.containerBtn} onPress={action}>
        <View style={styles.btn}>
          <Text style={styles.textBtn}>{text}</Text>
          <Icon name="arrow-forward" color="#4847B5" iconStyle={{marginTop: 5}} size={30} />
        </View>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  containerBtn: {
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#0f444c',
    backgroundColor: '#FFF',
    height: 45,
    width: 360,
    marginTop: 10,
    paddingHorizontal: 10,
  },
  btn: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  textBtn: {
    color: '#000000',
    marginVertical: 10,
    fontSize: 18,
    fontFamily: 'OpenSans-Regular',
    paddingLeft: 5,
  },
});
export default BtnOption;
