import React from 'react';
import {StyleSheet} from 'react-native';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const ProfileSkeleton = ({loading}) => {
  return (
    <SkeletonContent
      containerStyle={styles.skeletonArea}
      isLoading={loading}
      animationDirection="horizontalRight"
      layout={[{width: 390, height: 130, borderRadius: 10, marginTop: 10}]}
    />
  );
};

const styles = StyleSheet.create({
  skeletonArea: {
    height: 140,
    marginTop: 5,
    marginBottom: 10,
    alignItems: 'center',
  },
});

export default ProfileSkeleton;
