import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

const UserData = ({points, stars, reports, collector, avatar, misions}) => {
  return (
    <View style={styles.container}>
      <View style={styles.containerImg}>
        <Image
          style={styles.image}
          source={{
            uri: avatar,
          }}
        />
      </View>
      <View style={styles.containerData}>
        <Text style={styles.textTitle}>Hola, {collector.username}!</Text>
        <View style={styles.containerPoints}>
          <View style={styles.infoStyle}>
            <Text style={styles.textPoints}>{points}</Text>
            <Text style={styles.TextInfo}>Puntos</Text>
          </View>
          <View style={styles.infoStyle}>
            <Text style={styles.textPoints}>{reports}</Text>
            <Text style={styles.TextInfo}>Reportes</Text>
          </View>
          <View style={styles.infoStyle}>
            <Text style={styles.textPoints}>{stars}</Text>
            <Text style={styles.TextInfo}>Estrellas</Text>
          </View>
          <View style={styles.infoStyle}>
            <Text style={styles.textPoints}>{misions}</Text>
            <Text style={styles.TextInfo}>Misiones</Text>
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginTop: 5,
    marginBottom: 10,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    borderRadius: 20,
    flexDirection: 'row',
    height: 140,
    backgroundColor: '#4847B5',
    alignItems: 'center',
  },
  containerImg: {
    flex: 3,
  },
  image: {
    resizeMode: 'contain',
    width: '100%',
    height: 100,
  },
  containerData: {
    width: 265,
    marginTop: -10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
    marginVertical: 10,
  },
  containerPoints: {
    flexDirection: 'row',
  },
  infoStyle: {
    width: 60,
    borderRadius: 10,
    marginHorizontal: 3,
    height: 58,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  textPoints: {
    fontSize: 18,
    color: '#bf9704',
    fontFamily: 'OpenSans-SemiBold',
    textAlign: 'center',
  },
  TextInfo: {
    color: '#4847B5',
    textAlign: 'center',
    fontFamily: 'sans-serif',
    fontSize: 11,
  },
});
export default UserData;
