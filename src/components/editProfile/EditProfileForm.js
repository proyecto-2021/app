import React, {useState} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import {Input} from 'react-native-elements';
import {validateEmail, validateName} from '../../helpers/utils';

const EditProfileForm = ({variables, setVariables, updateProfile}) => {
  const [errorUsername, setErrorUsername] = useState('');
  const [errorFirstName, setErrorFirstName] = useState('');
  const [errorLastName, setErrorLastName] = useState('');
  const [errorEmail, setErrorEmail] = useState('');

  const validateData = () => {
    setErrorFirstName('');
    setErrorLastName('');
    setErrorEmail('');
    if (
      variables.firstName &&
      validateName(variables.firstName) &&
      variables.lastName &&
      validateName(variables.lastName) &&
      validateEmail(variables.email)
    ) {
      updateProfile();
    } else {
      variables.firstName ? '' : setErrorFirstName('Requerido');
      validateName(variables.firstName) ? '' : setErrorFirstName('Sin espacios, solo letras');
      variables.lastName ? '' : setErrorLastName('Requerido');
      validateName(variables.lastName) ? '' : setErrorLastName('Sin espacios, solo letras');
      validateEmail(variables.email) ? '' : setErrorEmail('Debes de ingresar un email válido.');
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.input}>
          <Input
            disabled
            containerStyle={styles.text}
            value={variables.username}
            onChange={val => setVariables({username: val.nativeEvent.text})}
            placeholder="Nombre de Usuario"
            placeholderTextColor="#4847B5"
            errorMessage={errorUsername}
            inputContainerStyle={styles.borderBottomWidth}
          />
        </View>

        <View style={styles.input}>
          <Input
            containerStyle={styles.text}
            value={variables.firstName}
            onChange={val => setVariables({firstName: val.nativeEvent.text})}
            placeholder="Nombre"
            placeholderTextColor="#4847B5"
            errorMessage={errorFirstName}
            inputContainerStyle={styles.borderBottomWidth}
          />
        </View>

        <View style={styles.input}>
          <Input
            containerStyle={styles.text}
            value={variables.lastName}
            onChange={val => setVariables({lastName: val.nativeEvent.text})}
            placeholder="Apellido"
            placeholderTextColor="#4847B5"
            errorMessage={errorLastName}
            inputContainerStyle={styles.borderBottomWidth}
          />
        </View>

        <View style={styles.input}>
          <Input
            containerStyle={styles.text}
            value={variables.email}
            onChange={val => setVariables({email: val.nativeEvent.text})}
            placeholder="Tucorreo@email.com"
            placeholderTextColor="#4847B5"
            errorMessage={errorEmail}
            inputContainerStyle={styles.borderBottomWidth}
          />
        </View>
      </ScrollView>
      <View style={styles.center}>
        {!variables.loading ? (
          <TouchableOpacity
            style={styles.btn}
            onPress={() => {
              validateData();
            }}>
            <Text style={styles.textBtn}>Guardar</Text>
          </TouchableOpacity>
        ) : (
          <ActivityIndicator size="large" color="#4847B5" />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    paddingHorizontal: 20,
  },
  input: {
    fontSize: 14,
    height: 45,
    paddingHorizontal: 5,
    color: '#000000',
    marginBottom: 20,
    borderRadius: 20,
    backgroundColor: '#FFF',
    borderColor: '#0f444c',
    borderWidth: 1,
  },
  text: {
    fontSize: 12,
    fontFamily: 'OpenSans-Regular',
    flex: 1,
  },
  btn: {
    padding: 10,
    backgroundColor: '#4847B5',
    marginVertical: 25,
    width: 130,
    borderRadius: 20,
  },
  textBtn: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 15,
    textAlign: 'center',
    color: '#ffffff',
  },
  center: {
    alignItems: 'center',
    marginBottom: 70,
  },
  borderBottomWidth: {
    borderBottomWidth: 0,
  },
});

export default EditProfileForm;
