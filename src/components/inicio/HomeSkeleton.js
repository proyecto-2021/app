import React from 'react';
import {StyleSheet, View} from 'react-native';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const HomeSkeleton = ({loading}) => {
  const creatureLayout = [
    {
      width: '60%',
      height: 120,
      marginTop: 10,
      borderRadius: 20,
    },
    {
      width: '50%',
      height: 10,
      marginVertical: 10,
    },
    {
      width: '70%',
      height: 10,
    },
    {
      width: '60%',
      height: 120,
      marginTop: 10,
      borderRadius: 20,
    },
    {
      width: '50%',
      height: 10,
      marginVertical: 10,
    },
    {
      width: '70%',
      height: 10,
    },
    {
      width: '60%',
      height: 120,
      marginTop: 10,
      borderRadius: 20,
    },
    {
      width: '50%',
      height: 10,
      marginVertical: 10,
    },
    {
      width: '70%',
      height: 10,
    },
  ];

  return (
    <View style={styles.main}>
      <View style={styles.skeletonArea}>
        <SkeletonContent isLoading={loading} layout={creatureLayout} />
      </View>
      <View style={styles.skeletonArea}>
        <SkeletonContent isLoading={loading} layout={creatureLayout} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  skeletonArea: {
    flex: 1,
  },
  main: {flexDirection: 'row', flex: 1, marginTop: '-20%'},
});

export default HomeSkeleton;
