import React, {useState} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import {validatePassword, validateNotBlank} from '../../helpers/utils';
import {Input} from 'react-native-elements';

const FormEditPassword = ({animating, updatePassword}) => {
  const [currentPassword, setCurrentPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [errorCurrentPassword, setErrorCurrentPassword] = useState('');
  const [errorNewPassword, setErrorNewPassword] = useState('');
  const [errorConfirmPassword, setErrorConfirmPassword] = useState('');

  const validateData = () => {
    setErrorCurrentPassword('');
    setErrorNewPassword('');
    setErrorConfirmPassword('');
    if (
      currentPassword &&
      validateNotBlank(newPassword) &&
      validatePassword(newPassword) &&
      newPassword === confirmPassword
    ) {
      const item = {
        currentPassword,
        newPassword,
        confirmPassword,
      };
      updatePassword(item);
    } else {
      currentPassword ? '' : setErrorCurrentPassword('Ingrese contraseña actual');
      validatePassword(newPassword)
        ? ''
        : setErrorNewPassword('La contraseña debe ser de 8 carácteres.');
      validateNotBlank(newPassword) ? '' : setErrorNewPassword('Sin espacios');
      newPassword === confirmPassword
        ? ''
        : setErrorConfirmPassword('Las contraseñas no coinciden');
    }
  };

  const closeKeyboard = () => {
    Keyboard.dismiss();
  };

  return (
    <TouchableWithoutFeedback onPress={() => closeKeyboard()}>
      <View style={styles.container}>
        <View>
          <Text style={styles.textPassword}>Ingresa la nueva contraseña</Text>
        </View>
        <View style={styles.input}>
          <Input
            containerStyle={styles.text}
            onChange={ev => setCurrentPassword(ev.nativeEvent.text)}
            placeholder="Contraseña actual"
            errorMessage={errorCurrentPassword}
            secureTextEntry={true}
            inputContainerStyle={styles.borderBottomWidth}
          />
        </View>
        <View style={styles.input}>
          <Input
            containerStyle={styles.text}
            onChange={ev => setNewPassword(ev.nativeEvent.text)}
            placeholder="Nueva contraseña"
            errorMessage={errorNewPassword}
            secureTextEntry={true}
            inputContainerStyle={styles.borderBottomWidth}
          />
        </View>
        <View style={styles.input}>
          <Input
            containerStyle={styles.text}
            onChange={ev => setConfirmPassword(ev.nativeEvent.text)}
            placeholder="Confirmar contraseña"
            errorMessage={errorConfirmPassword}
            secureTextEntry={true}
            inputContainerStyle={styles.borderBottomWidth}
          />
        </View>
        <View style={styles.center}>
          {animating ? (
            <View style={styles.loading}>
              <ActivityIndicator size="large" color="#0f444c" animating={animating} />
            </View>
          ) : (
            <TouchableOpacity
              style={styles.btn}
              onPress={() => {
                validateData();
              }}>
              <Text style={styles.textBtn}>Confirmar</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  textPassword: {
    color: '#4847B5',
    fontSize: 20,
    fontFamily: 'OpenSans-Bold',
    paddingVertical: 20,
    textAlign: 'center',
  },
  input: {
    fontSize: 14,
    flexDirection: 'row',
    height: 45,
    paddingHorizontal: 5,
    color: '#FFF',
    textDecorationLine: 'none',
    marginBottom: 22,
    borderRadius: 20,
    backgroundColor: '#FFF',
    borderColor: '#0f444c',
    borderWidth: 1,
    borderBottomWidth: 1,
  },
  text: {
    fontFamily: 'OpenSans-Regular',
  },
  center: {
    alignItems: 'center',
  },
  btn: {
    padding: 10,
    backgroundColor: '#4847B5',
    marginVertical: 150,
    width: 130,
    borderRadius: 20,
  },
  textBtn: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 15,
    textAlign: 'center',
    color: '#ffffff',
  },
  loading: {
    justifyContent: 'center',
    marginTop: 150,
  },
  borderBottomWidth: {
    borderBottomWidth: 0,
  },
});

export default FormEditPassword;
