import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

const LeaderBoardHeader = () => {
  return (
    <View>
      <Image
        style={styles.medalImages}
        source={require('../../assets/resources/leaderboard/place.png')}
      />
      <View style={styles.viewTitle}>
        <Text style={styles.position}>°</Text>
        <Text style={styles.username}>Recolector</Text>
        <Text style={styles.features}>Ptos</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  medalImages: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 102,
    backgroundColor: '#4847B5',
  },
  position: {
    flex: 0.2,
    marginHorizontal: '4%',
    marginTop: 15,
    marginBottom: 3,
    fontSize: 17,
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'OpenSans-Bold',
    justifyContent: 'center',
    backgroundColor: '#4847B5',
  },
  username: {
    flex: 0.5,
    marginVertical: 15,
    fontSize: 17,
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'OpenSans-Bold',
    justifyContent: 'center',
  },
  features: {
    flex: 0.3,
    marginHorizontal: '2%',
    marginVertical: 15,
    fontSize: 17,
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'OpenSans-Bold',
    justifyContent: 'center',
  },
  viewTitle: {
    flexDirection: 'row',
    backgroundColor: '#4847B5',
    borderColor: '#8aa39b',
    borderBottomWidth: 2,
  },
});
export default LeaderBoardHeader;
