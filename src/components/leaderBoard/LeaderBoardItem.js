import React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';

const LeaderBoardItem = ({item}) => {
  return (
    <View style={styles.itemTable}>
      {item.position === 1 ? (
        <Image
          style={styles.medalImages}
          source={require('../../assets/resources/leaderboard/gold.png')}
        />
      ) : item.position === 2 ? (
        <Image
          style={styles.medalImages}
          source={require('../../assets/resources/leaderboard/silver.png')}
        />
      ) : item.position === 3 ? (
        <Image
          style={styles.medalImages}
          source={require('../../assets/resources/leaderboard/bronce.png')}
        />
      ) : (
        <Text style={styles.regularPosition}>{item.position}</Text>
      )}
      <Text style={styles.username}>{item.user.username}</Text>
      <Text style={styles.features}>{item.score}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  itemTable: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: '2%',
  },
  regularPosition: {
    flex: 0.2,
    marginHorizontal: '4%',
    marginVertical: 15,
    fontSize: 15,
    textAlign: 'center',
    color: '#8aa39b',
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  username: {
    flex: 0.5,
    marginVertical: 15,
    fontSize: 15,
    textAlign: 'center',
    color: '#000',
    fontFamily: 'OpenSans-SemiBold',
    justifyContent: 'center',
  },
  features: {
    flex: 0.3,
    marginHorizontal: '2%',
    marginVertical: 15,
    fontSize: 15,
    textAlign: 'center',
    color: '#000',
    fontFamily: 'OpenSans-SemiBold',
    justifyContent: 'center',
  },
  medalImages: {
    flex: 0.2,
    marginHorizontal: '4%',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 42,
  },
});

export default LeaderBoardItem;
