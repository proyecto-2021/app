import React from 'react';
import {View, Text, StyleSheet, ScrollView, TextInput, TouchableOpacity, Image} from 'react-native';
import Textarea from 'react-native-textarea';

const ReportDetail = ({item, setModalDetail}) => {
  const hideModal = () => {
    setModalDetail(false);
  };

  console.log(item.classificationReport);
  return (
    <>
      <ScrollView style={styles.modalPrincipal}>
        <View style={styles.modalSecondary}>
          <Text style={styles.title}>Detalle de Reporte {item.id}</Text>
          <View style={styles.container}>
            <Text style={styles.text}>Descripción</Text>
            <Textarea
              containerStyle={styles.textAreaContainer}
              style={styles.textArea}
              defaultValue={item.description}
              placeholderTextColor={'#000000'}
              editable={false}
            />
          </View>
          {item.classificationReport ? (
            <View>
              <Text style={styles.text}>Clasificación</Text>
              <TextInput
                style={styles.input}
                value={
                  item.classificationReport === 'L'
                    ? 'Baja'
                    : item.classificationReport === 'M'
                    ? 'Media'
                    : item.classificationReport === 'H'
                    ? 'Alta'
                    : null
                }
                editable={false}
              />
            </View>
          ) : null}

          <View>
            <Text style={styles.text}>Estatus</Text>
            <TextInput
              style={styles.input}
              value={
                item.status === 'P'
                  ? 'Pendiente'
                  : item.status === 'PR'
                  ? 'Proceso'
                  : item.status === 'PB'
                  ? 'Pruebas'
                  : item.status === 'PO'
                  ? 'Procesado'
                  : item.status === 'R'
                  ? 'Rechazado'
                  : null
              }
              editable={false}
            />
          </View>
          <View>
            <Text style={styles.text}>Puntuación</Text>
            <TextInput
              style={styles.input}
              value={
                item.status === 'PO'
                  ? item.classificationReport === 'L'
                    ? '20 Puntos'
                    : item.classificationReport === 'M'
                    ? '30 Puntos'
                    : item.classificationReport === 'H'
                    ? '40 Puntos'
                    : null
                  : 'No Asignado'
              }
              editable={false}
            />
          </View>
          <View>
            <Text style={styles.text}>Versión de la Criatura</Text>
            <TextInput style={styles.input} value={item.creatureVersion.name} editable={false} />
          </View>
          <View>
            <Text style={styles.text}>URL de la Criatura</Text>
            <TextInput style={styles.input} value={item.creatureVersion.url} editable={false} />
          </View>
          <View>
            <Text style={styles.text}>Fecha</Text>
            <TextInput style={styles.input} value={item.date} editable={false} />
          </View>
          {item.image ? (
            <View>
              <Text style={styles.text}>Imagen</Text>
              <View style={styles.containerImage}>
                <Image
                  style={styles.imagenPick}
                  source={{
                    uri: item.image,
                  }}
                />
              </View>
            </View>
          ) : null}
          <View style={styles.btnCenter}>
            <TouchableOpacity onPress={hideModal} style={styles.btn}>
              <Text style={styles.textBtn}> Cerrar </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  modalPrincipal: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  modalSecondary: {
    backgroundColor: '#f8f8f8',
    margin: 20,
    paddingTop: 10,
    borderRadius: 20,
    flex: 1,
    paddingHorizontal: 20,
  },
  title: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 20,
    marginTop: 10,
    textAlign: 'center',
    color: '#4847B5',
  },
  text: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 17,
    marginTop: 10,
  },
  input: {
    marginTop: 5,
    height: 43,
    borderColor: '#D1D1D1',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 20,
    paddingHorizontal: 13,
    color: '#4847B5',
    fontFamily: 'OpenSans-SemiBold',
  },
  btn: {
    padding: 10,
    backgroundColor: '#4847B5',
    marginVertical: 15,
    width: 130,
    borderRadius: 20,
  },
  btnCenter: {
    alignItems: 'center',
  },
  textBtn: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 17,
    textAlign: 'center',
    color: '#ffffff',
  },
  container: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    paddingVertical: 10,
  },
  textAreaContainer: {
    height: 160,
    paddingVertical: 10,
  },
  textArea: {
    textAlignVertical: 'top',
    height: 150,
    fontSize: 14,
    borderColor: '#D1D1D1',
    color: '#4847B5',
    borderWidth: 1,
    borderRadius: 20,
    paddingHorizontal: 10,
    fontFamily: 'OpenSans-SemiBold',
  },
  containerImage: {
    alignItems: 'center',
    position: 'relative',
    marginTop: 15,
  },
  imagenPick: {
    width: '100%',
    height: 350,
    resizeMode: 'stretch',
    borderRadius: 20,
    marginBottom: 10,
  },
});

export default ReportDetail;
