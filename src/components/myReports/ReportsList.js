import React, {useState} from 'react';
import {View, Text, StyleSheet, ScrollView, TouchableOpacity, Modal} from 'react-native';
import ReportDetail from './ReportDetail';

const ReportsList = ({item}) => {
  const [modalDetail, setModalDetail] = useState(false);

  const showModal = () => {
    setModalDetail(true);
  };
  return (
    <ScrollView style={styles.container}>
      <Text style={styles.textDescription}> {item.description} </Text>
      <Text style={styles.textType}>
        {' '}
        {item.type === 'E' ? 'Reporte de Error' : 'Reporte de Mejora'}
      </Text>
      <View style={styles.containerButton}>
        <Text style={styles.textDate}> {item.date} </Text>
        <TouchableOpacity onPress={showModal} style={styles.button}>
          <Text style={styles.lblButton}> Detalle </Text>
          <Modal transparent={true} visible={modalDetail}>
            <ReportDetail item={item} setModalDetail={setModalDetail} />
          </Modal>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    paddingHorizontal: 5,
    paddingVertical: 10,
    borderColor: '#0f444c',
    borderWidth: 1,
    borderRadius: 20,
    marginBottom: 10,
  },
  containerButton: {
    marginTop: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  button: {
    backgroundColor: '#4847B5',
    width: 100,
    height: 30,
    borderRadius: 20,
    marginRight: 5,
  },
  lblButton: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 15,
    textAlign: 'center',
    color: '#ffffff',
    marginTop: 4,
  },
  textDescription: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 17,
  },
  textType: {
    fontFamily: 'OpenSans-Italic',
    fontSize: 13,
  },
  textDate: {
    fontFamily: 'OpenSans-Regular',
    fontSize: 12,
    textAlign: 'right',
    paddingTop: 10,
  },
});

export default ReportsList;
