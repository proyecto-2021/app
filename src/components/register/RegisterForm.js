import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import {Input} from 'react-native-elements';
import {validateEmail, validatePassword, validateNotBlank, validateName} from '../../helpers/utils';

const RegisterForm = ({variables, setVariables, registerUser}) => {
  const [errorUsername, setErrorUsername] = useState('');
  const [errorFirstName, setErrorFirstName] = useState('');
  const [errorLastName, setErrorLastName] = useState('');
  const [errorEmail, setErrorEmail] = useState('');
  const [errorPassword, setErrorPassword] = useState('');
  const [errorConfirmPassword, setErrorConfirmPassword] = useState('');
  const [passValidate, setPassValidate] = useState(false);

  const validateData = () => {
    setErrorUsername('');
    setErrorFirstName('');
    setErrorLastName('');
    setErrorEmail('');
    setErrorPassword('');
    setErrorConfirmPassword('');
    setPassValidate(false);
    if (
      variables.username &&
      validateNotBlank(variables.username) &&
      variables.firstName &&
      validateName(variables.firstName) &&
      variables.lastName &&
      validateName(variables.lastName) &&
      validateEmail(variables.email) &&
      validateNotBlank(variables.password) &&
      validatePassword(variables.password) &&
      variables.password === variables.confirmPassword
    ) {
      registerUser();
    } else {
      variables.username ? '' : setErrorUsername('Requerido');
      validateNotBlank(variables.username) ? '' : setErrorUsername('Sin espacios');
      variables.firstName ? '' : setErrorFirstName('Requerido');
      validateName(variables.firstName) ? '' : setErrorFirstName('Sin espacios, solo letras');
      variables.lastName ? '' : setErrorLastName('Requerido');
      validateName(variables.lastName) ? '' : setErrorLastName('Sin espacios, solo letras');
      validateEmail(variables.email) ? '' : setErrorEmail('Debes ingresar un email válido');
      // validatePassword(variables.password) ? '' : setErrorPassword('La contraseña debe ser de 8 carácteres.');
      validatePassword(variables.password) ? '' : setPassValidate(true);
      validateNotBlank(variables.password) ? '' : setErrorPassword('Sin espacios');
      variables.password === variables.confirmPassword
        ? ''
        : setErrorConfirmPassword('Las contraseñas no coinciden');
    }
  };

  return (
    <ScrollView>
      <View style={styles.input}>
        <Input
          containerStyle={styles.textPlaceholder}
          onChange={val => setVariables({username: val.nativeEvent.text})}
          placeholder="Nombre de Usuario"
          placeholderTextColor="#4847B5"
          errorMessage={errorUsername}
          inputContainerStyle={styles.borderBottomWidth}
        />
      </View>
      <View style={styles.input}>
        <Input
          containerStyle={styles.textPlaceholder}
          onChange={val => setVariables({firstName: val.nativeEvent.text})}
          placeholder="Nombre"
          placeholderTextColor="#4847B5"
          errorMessage={errorFirstName}
          inputContainerStyle={styles.borderBottomWidth}
        />
      </View>
      <View style={styles.input}>
        <Input
          containerStyle={styles.textPlaceholder}
          onChange={val => setVariables({lastName: val.nativeEvent.text})}
          placeholder="Apellido"
          placeholderTextColor="#4847B5"
          errorMessage={errorLastName}
          inputContainerStyle={styles.borderBottomWidth}
        />
      </View>
      <View style={styles.input}>
        <Input
          containerStyle={styles.textPlaceholder}
          onChange={val => setVariables({email: val.nativeEvent.text})}
          placeholder="Tucorreo@email.com"
          placeholderTextColor="#4847B5"
          errorMessage={errorEmail}
          inputContainerStyle={styles.borderBottomWidth}
        />
      </View>
      <View style={styles.input}>
        <Input
          containerStyle={styles.textPlaceholder}
          onChange={val => setVariables({password: val.nativeEvent.text})}
          placeholder="Contraseña"
          placeholderTextColor="#4847B5"
          errorMessage={errorPassword}
          secureTextEntry={true}
          inputContainerStyle={styles.borderBottomWidth}
        />
      </View>
      {passValidate ? (
        <View style={styles.passWContainer}>
          <Text style={styles.passW}>
            {' '}
            La contraseña debe poseer al menos 8 caracteres, alguna letra mayuscula, algun número y
            un caracter especial{' '}
          </Text>
        </View>
      ) : null}
      <View style={styles.input}>
        <Input
          containerStyle={styles.textPlaceholder}
          onChange={val => setVariables({confirmPassword: val.nativeEvent.text})}
          placeholder="Repetir contraseña"
          placeholderTextColor="#4847B5"
          errorMessage={errorConfirmPassword}
          secureTextEntry={true}
          inputContainerStyle={styles.borderBottomWidth}
        />
      </View>

      <View style={styles.center}>
        {!variables.loading ? (
          <TouchableOpacity
            style={styles.btnRegister}
            onPress={() => {
              validateData();
            }}>
            <Text style={styles.lblBtn}>Registrar</Text>
          </TouchableOpacity>
        ) : (
          <ActivityIndicator size="large" color="#FFF" animating={variables.loading} />
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
    marginBottom: 20,
  },
  input: {
    fontSize: 10,
    height: 50,
    paddingHorizontal: 5,
    textDecorationLine: 'none',
    marginBottom: 15,
    borderRadius: 30,
    backgroundColor: '#FFF',
    borderColor: '#4847B5',
    borderWidth: 3,
  },
  textPlaceholder: {
    fontSize: 10,
    fontFamily: 'OpenSans-Regular',
    flex: 1,
    color: '#000000',
  },
  btnRegister: {
    borderColor: '#fff',
    borderWidth: 0.3,
    borderRadius: 20,
    backgroundColor: '#4847B5',
    height: 45,
    width: '100%',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lblBtn: {
    color: '#FFF',
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
    marginVertical: 5,
  },
  borderBottomWidth: {
    borderBottomWidth: 0,
  },
  passWContainer: {
    marginTop: -12,
    marginHorizontal: 20,
  },
  passW: {
    color: 'red',
    fontSize: 11.5,
  },
});

export default RegisterForm;
