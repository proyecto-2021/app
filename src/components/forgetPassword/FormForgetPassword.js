import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Input} from 'react-native-elements';
import {validateEmail} from '../../helpers/utils';

const FormForgetPassword = ({loading, fogetPassword}) => {
  const [email, setEmail] = useState('');
  const [errorEmail, setErrorEmail] = useState('');

  const validateData = () => {
    setErrorEmail('');
    if (validateEmail(email)) {
      const item = {
        email,
      };
      fogetPassword(item);
    } else {
      validateEmail(email) ? '' : setErrorEmail('Debes de ingresar un email válido.');
    }
  };
  return (
    <SafeAreaView style={styles.contain}>
      <View>
        <Text style={styles.lblForget}>
          Ingresa tu correo electrónico previamente registrado en Agatha.
        </Text>
      </View>
      <View style={styles.input}>
        <Input
          containerStyle={styles.textPlaceholder}
          onChange={ev => setEmail(ev.nativeEvent.text)}
          placeholder="Tucorreo@email.com"
          errorMessage={errorEmail}
        />
      </View>
      <View style={styles.center}>
        {!loading ? (
          <TouchableOpacity
            style={styles.btnLog}
            onPress={() => {
              validateData();
            }}>
            <Text style={styles.lblBtn}>Enviar</Text>
          </TouchableOpacity>
        ) : (
          <ActivityIndicator size="large" color="#FFF" />
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
  },
  center: {
    alignItems: 'center',
  },
  input: {
    flexDirection: 'row',
    height: 55,
    paddingHorizontal: 10,
    color: '#FFF',
    textDecorationLine: 'none',
    marginBottom: 15,
    borderRadius: 30,
    backgroundColor: '#FFF',
    borderColor: '#4847B5',
    borderWidth: 3,
  },
  lblForget: {
    color: '#FFF',
    fontSize: 20,
    fontFamily: 'OpenSans-Bold',
    paddingTop: 40,
    paddingBottom: 40,
    textAlign: 'center',
  },
  lblBtn: {
    color: '#FFF',
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
    marginVertical: 10,
  },
  btnLog: {
    borderColor: '#fff',
    borderWidth: 0.3,
    borderRadius: 20,
    backgroundColor: '#4847B5',
    height: 45,
    width: 160,
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default FormForgetPassword;
