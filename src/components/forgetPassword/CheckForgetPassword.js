import React from 'react';
import {View, StyleSheet, Text, SafeAreaView, TouchableOpacity} from 'react-native';

const CheckForgetPassword = ({navigation}) => {
  return (
    <SafeAreaView style={styles.contain}>
      <View>
        <Text style={styles.lblForget}>
          Te hemos enviado un correo con tu nueva contraseña, revisa tu bandeja de entrada.
        </Text>
      </View>
      <View style={styles.center}>
        <TouchableOpacity
          style={styles.btnLog}
          onPress={() => {
            navigation.goBack();
          }}>
          <Text style={styles.lblBtn}>Inicio</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
  },
  center: {
    alignItems: 'center',
  },
  lblForget: {
    color: '#FFF',
    fontSize: 20,
    fontFamily: 'OpenSans-Bold',
    paddingTop: 40,
    paddingBottom: 40,
    textAlign: 'center',
  },
  lblBtn: {
    color: '#FFF',
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
    marginVertical: 10,
  },
  btnLog: {
    borderRadius: 20,
    backgroundColor: '#4847B5',
    height: 45,
    width: 160,
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default CheckForgetPassword;
