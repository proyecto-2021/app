import React from 'react';
import {SafeAreaView, FlatList, StyleSheet} from 'react-native';
import Notification from './Notification';

const NotificationsList = ({notifications}) => {
  const renderItem = ({item}) => <Notification notification={item} />;
  return (
    <SafeAreaView>
      <FlatList
        data={notifications}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        style={styles.flat}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  flat: {
    marginBottom: 15,
  },
});

export default NotificationsList;
