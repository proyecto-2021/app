// notification
import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

const Notification = ({notification}) => (
  <TouchableOpacity style={styles.button}>
    <View style={styles.newNotification}>
      <Text style={styles.title}>{notification.name}</Text>
      <Text style={styles.description}>{notification.description}</Text>
      <Text style={styles.dateTime}>{notification.date}</Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  newNotification: {
    backgroundColor: '#ffffff',
    padding: 10,
    borderColor: '#D1D1D1',
    borderWidth: 1,
    borderRadius: 20,
  },
  title: {
    fontFamily: 'OpenSans-SemiBoldItalic',
    fontSize: 17,
  },
  description: {
    fontFamily: 'OpenSans-Regular',
    fontSize: 13,
  },
  dateTime: {
    fontFamily: 'OpenSans-Regular',
    fontSize: 12,
    textAlign: 'right',
    paddingTop: 10,
  },
  button: {
    marginBottom: 10,
  },
});

export default Notification;
