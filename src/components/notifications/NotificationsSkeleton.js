import React from 'react';
import {StyleSheet} from 'react-native';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const ReportsSkeleton = ({loading}) => {
  return (
    <SkeletonContent
      containerStyle={styles.skeletonArea}
      isLoading={loading}
      animationDirection="horizontalRight"
      layout={[
        {width: 395, height: 90, marginBottom: 7, borderRadius: 20},
        {width: 395, height: 90, marginBottom: 7, borderRadius: 20},
        {width: 395, height: 90, marginBottom: 7, borderRadius: 20},
        {width: 395, height: 90, marginBottom: 7, borderRadius: 20},
        {width: 395, height: 90, marginBottom: 7, borderRadius: 20},
        {width: 395, height: 90, marginBottom: 7, borderRadius: 20},
      ]}
    />
  );
};

const styles = StyleSheet.create({
  skeletonArea: {
    flex: 1,
    width: 300,
  },
});

export default ReportsSkeleton;
