import {Platform} from 'react-native';

export function createFormData(photo, body) {
  const data = new FormData();
  if (photo) {
    data.append('file', {
      name: photo.fileName,
      type: photo.type,
      uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
    });
  }
  Object.keys(body).map(key => {
    if (Array.isArray(body[key])) {
      body[key].forEach(val => {
        if (typeof val === 'object' && val !== null) {
          return data.append(`${key}[]`, JSON.stringify(val));
        }
        return data.append(`${key}[]`, val);
      });
    } else if (typeof body[key] === 'object' && body[key] !== null) {
      return data.append(key, JSON.stringify(body[key]));
    } else {
      return data.append(key, body[key]);
    }
  });

  return data;
}
