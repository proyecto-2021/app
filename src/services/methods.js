import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import UserAgent from 'react-native-user-agent';
import publicIP from 'react-native-public-ip';
// let path = 'https://agatha-develop.herokuapp.com/api/'; //Develop
let path = 'http://dev.najoconsultores.com:20044/api/'; //Prod
const userAgent = UserAgent.getUserAgent();

export default {
  async get(url) {
    let headers;
    let token = await AsyncStorage.getItem('token');
    if (token) {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: 'Bearer ' + token,
        'User-Agent': userAgent,
      };
    } else {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'User-Agent': userAgent,
      };
    }
    return new Promise((resolve, reject) => {
      axios({method: 'get', url: path + url, headers: headers})
        .then(res => {
          resolve(res.data);
        })
        .catch(err => reject(err));
    });
  },
  async post(url, data) {
    let headers;
    let token = await AsyncStorage.getItem('token');
    if (token) {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: 'Bearer ' + token,
        'User-Agent': userAgent,
        'Public-ip': await publicIP(),
      };
    } else {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'User-Agent': userAgent,
      };
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: path + url,
        data: data,
        headers: headers,
      })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  async put(url, data) {
    let headers;
    let token = await AsyncStorage.getItem('token');
    if (token) {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: 'Bearer ' + token,
        'User-Agent': userAgent,
      };
    } else {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'User-Agent': userAgent,
      };
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'put',
        url: path + url,
        data: data,
        headers: headers,
      })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => reject(err));
    });
  },
  async delete(url) {
    let headers;
    let token = await AsyncStorage.getItem('token');
    if (token) {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: 'Bearer ' + token,
        'User-Agent': userAgent,
      };
    } else {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'User-Agent': userAgent,
      };
    }
    return new Promise((resolve, reject) => {
      axios({method: 'delete', url: path + url, headers: headers})
        .then(res => {
          resolve(res.data);
        })
        .catch(err => reject(err));
    });
  },
  async patch(url, data) {
    let headers;
    let token = await AsyncStorage.getItem('token');
    if (token) {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: 'Bearer ' + token,
        'User-Agent': userAgent,
      };
    } else {
      headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      };
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'patch',
        url: path + url,
        data: data,
        headers: headers,
      })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
};
