import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Routes from './Routes';

const Stack = createStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: '#4847B5',
  },
  headerTintColor: '#ffffff',
  headerBackTitle: '#ffffff',
  headerTitleStyle: {
    fontFamily: 'OpenSans-Bold',
  },
};

const HomeNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Home" component={Routes.Home} options={{title: 'Inicio'}} />
      <Stack.Screen name="Creature" component={Routes.Creature} options={{title: 'Criatura'}} />
      <Stack.Screen
        name="CreateReport"
        component={Routes.CreateReport}
        options={{title: 'Crear Reporte'}}
      />
    </Stack.Navigator>
  );
};

const ReportsNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Reports" component={Routes.Reports} options={{title: 'Mis Reportes'}} />
    </Stack.Navigator>
  );
};

const LeaderBoardNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="LeaderBoard"
        component={Routes.LeaderBoard}
        options={{title: 'Tabla de Clasificación'}}
      />
    </Stack.Navigator>
  );
};

const NotificationsNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="Notifications"
        component={Routes.Notifications}
        options={{title: 'Notificaciones'}}
      />
    </Stack.Navigator>
  );
};

const ProfileNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Profile" component={Routes.Profile} options={{title: 'Perfil'}} />
      <Stack.Screen
        name="EditPassword"
        component={Routes.EditPassword}
        options={{title: 'Editar Contraseña'}}
      />
      <Stack.Screen
        name="EditProfile"
        component={Routes.EditProfile}
        options={{title: 'Editar Perfil'}}
      />
      <Stack.Screen name="Rewards" component={Routes.Rewards} options={{title: 'Recompensas'}} />
    </Stack.Navigator>
  );
};

export {
  HomeNavigator,
  LeaderBoardNavigator,
  ReportsNavigator,
  NotificationsNavigator,
  ProfileNavigator,
};
