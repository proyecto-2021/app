import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Routes from './Routes';

const AuthStack = createStackNavigator();

const AuthNavigator = () => {
  const Login = () => {
    return (
      <AuthStack.Navigator>
        <AuthStack.Screen
          name="Login"
          component={Routes.Login}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="Register"
          component={Routes.Register}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="ForgetPassword"
          component={Routes.ForgetPassword}
          options={{
            headerShown: false,
          }}
        />
      </AuthStack.Navigator>
    );
  };

  return <Login />;
};

export default AuthNavigator;
