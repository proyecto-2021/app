import Home from '../views/Home';
import LeaderBoard from '../views/LeaderBoard';
import Notifications from '../views/Notifications';
import Profile from '../views/Profile';
import Reports from '../views/Reports';
import Login from '../views/Login';
import Register from '../views/Register';

import CreateReport from '../views/CreateReport';
import Creature from '../views/Creature';
import EditPassword from '../views/EditPassword';
import EditProfile from '../views/EditProfile';
import ForgetPassword from '../views/ForgetPassword';
import Rewards from '../views/Rewards';

export default {
  Home,
  LeaderBoard,
  Notifications,
  Profile,
  Reports,
  Login,
  Register,
  ForgetPassword,
  CreateReport,
  Creature,
  EditPassword,
  EditProfile,
  Rewards,
};
