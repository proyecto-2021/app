import React from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  HomeNavigator,
  LeaderBoardNavigator,
  ProfileNavigator,
  ReportsNavigator,
  NotificationsNavigator,
} from './StackNavigator';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  const TabOption = ({icon, focus}) => {
    return (
      <View style={styles.center}>
        <Ionicons name={icon} size={30} color={focus ? '#fff' : '#A0A0C0'} />
      </View>
    );
  };

  const TabCenterOption = ({icon}) => {
    return (
      <View style={styles.center}>
        <Ionicons name={icon} size={30} color={'#fff'} style={{transform: [{rotate: '-45deg'}]}} />
      </View>
    );
  };

  const TabCenterButtom = ({children, onPress}) => {
    return (
      <TouchableOpacity onPress={onPress} style={[styles.tabButtomCenter, styles.shadow]}>
        <View style={styles.tabCenter}>{children}</View>
      </TouchableOpacity>
    );
  };

  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        showLabel: false,
        style: {
          position: 'absolute',
          elevation: 0,
          backgroundColor: '#4847B5',
          height: 70,
        },
      }}>
      <Tab.Screen
        name="Reports"
        component={ReportsNavigator}
        options={{
          tabBarIcon: ({focused}) => <TabOption icon={'list-circle'} focus={focused} />,
        }}
      />
      <Tab.Screen
        name="LeaderBoard"
        component={LeaderBoardNavigator}
        options={{
          tabBarIcon: ({focused}) => <TabOption icon={'stats-chart'} focus={focused} />,
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeNavigator}
        options={{
          tabBarIcon: ({focused}) => <TabCenterOption icon={'home'} />,
          tabBarButton: props => <TabCenterButtom {...props} />,
        }}
      />
      <Tab.Screen
        name="Notifications"
        component={NotificationsNavigator}
        options={{
          tabBarIcon: ({focused}) => <TabOption icon={'notifications'} focus={focused} />,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileNavigator}
        options={{
          tabBarIcon: ({focused}) => <TabOption icon={'person'} focus={focused} />,
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#4847B5',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5,
  },
  center: {alignItems: 'center', justifyContent: 'center'},
  tabButtomCenter: {
    top: -20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabCenter: {
    width: 60,
    height: 60,
    borderRadius: 10,
    borderColor: '#fff',
    borderWidth: 5,
    backgroundColor: '#4847B5',
    transform: [{rotate: '45deg'}],
  },
});

export default BottomTabNavigator;
