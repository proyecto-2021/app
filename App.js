import React, {useState, useEffect, useMemo} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import SplashScreen from './src/views/SplashScreen';
import BottomTabNavigator from './src/navigation/TabNavigator';
import AuthNavigator from './src/navigation/AuthNavigator';
import AppContext from './src/helpers/context';
import AsyncStorage from '@react-native-community/async-storage';

const App = () => {
  const [isLoading, setisLoading] = useState(true);
  const [userToken, setuserToken] = useState(null);

  const authContext = useMemo(() => {
    return {
      signIn: () => {
        setisLoading(false);
        setuserToken('log');
      },
      signUp: () => {
        setisLoading(false);
        setuserToken('log');
      },
      signOut: () => {
        setisLoading(false);
        setuserToken(null);
      },
    };
  }, []);

  const verificarStorage = async () => {
    let token = await AsyncStorage.getItem('token');
    token ? setuserToken(token) : null;
  };

  useEffect(() => {
    verificarStorage();
    setTimeout(() => {
      setisLoading(false);
    }, 6000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }
  return (
    <AppContext.Provider value={authContext}>
      <NavigationContainer>
        {userToken ? <BottomTabNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </AppContext.Provider>
  );
};

export default App;
