# AGATHA 0.0.1
# Repositorio Mobile. Sistemas de información 2021
Entorno mobile. Proyecto de sistemas de información. Proyecto que se realizará por los estudiantes del décimo semestre de ingeniería Informática de la UCLA.

## Integrantes de Equipo
- Jesús Abrahán Peña(Líder de Equipo)
- Rhonald Sánchez
- Daniela Cadenas
- José Gabriel Guillén
- Nathalie Escalona

### Especificaciones técnicas / Referencias
- Documentación oficial `https://reactnative.dev/docs/getting-started`

#### Requerimientos previos:
- NodeJS
- Npm
- react-native      ref: `https://github.com/react-native-community/cli/blob/master/README.md#creating-a-new-react-native-project`

### Configurar el entorno de desarrollo
#### Android environment
- Android Studio
- Android SDK
- Android SDK Platform (30)
- Android Virtual Device
- Ref: `https://reactnative.dev/docs/environment-setup`

#### Comandos para desplegar funciones.
|°|Comando|Descripción|Notas|
|--|--|--|--|
|1|`npm run android`| Construcción de la aplicación en emulador Android. | # <=> react-native run-android|
|2|`npm run ios`| Construcción de la aplicación en emulador (xcode) iOS. |  # <=> react-native run-ios |
|3|`npm run restart`| Reinicio de la app con un nuevo cache | # <=> react-native start --reset-cache |
|3|`npm run build-android`| Compilación de codigo fuente para Android | # compile source code |
|3|`npm run build-ios `| Compilación de codigo fuente para iOS | # compile source code |

#### Build para pruebas (CASO EXLUSIVO DE ANDROID)
- Nota: Para caso de prueba, correr el comando `npm run bundle-android`, ir al directorio android con `cd android` y correr el comando `./gradlew assembleDebug`. Esto generara un apk sin firma para las pruebas android en la carpeta: `project/android/app/build/outputs/apk/debug/app-debug.apk`
- Actualmente no disponible para entorno iOS.

### Folder Structure
```
├──package.json
├──App.js
└──src:
    ├──assets:
    │   ├──gif
    │   ├──fonts:
    │   │   └── ...
    │   ├──icons:
    │   │   ├── ...
    │   └──resources:
    │       ├── ...
    ├──components:
    │   ├── ...
    ├──helpers:
    │   ├── ...
    ├──navigation:
    │   ├── ...
    ├──services:
    │   ├── ...
    ├──views:
    │   ├── ...
```

### Notas
- La aplicación fue creada por medio de react-native cli.
- Paquetes y dependecias utilizadas para la elaboración:

|°|Paquete|Versión|
|--|--|--|
|1|`react-native`| `^0.63.4`|
|2|`react-native`| `^0.63.4`|
|3|`react-native-elements`| `^3.4.2`|
|4|`react-native-gesture-handler`| `^1.10.3`|
|5|`react-native-image-picker`| `^4.0.6`|
|6|`react-native-linear-gradient`| `^2.5.6`|
|7|`react-native-modal-datetime-picker`| `^9.2.3`|
|8|`react-native-normalize`| `^1.0.0`|
|9|`react-native-progress`| `^4.1.2`|
|10|`react-native-reanimated`| `^2.1.0`|
|11|`react-native-safe-area-context`| `^3.2.0`|
|12|`react-native-screens`| `^3.3.0`|
|13|`react-native-skeleton-content-nonexpo`| `^1.0.13`|
|14|`react-native-textarea`| `^1.0.4`|
|15|`react-native-vector-icons`| `^8.1.0`|
|16|`@react-native-community/async-storage`| `^1.12.1`|
|17|`@react-native-community/checkbox`| `^0.5.8`|
|18|`@react-native-community/datetimepicker`| `^3.5.0`|
|19|`@react-native-community/masked-view`| `^0.1.11`|
|20|`@react-native-picker/picker`| `^1.16.1`|
|21|`@react-navigation/bottom-tabs`| `^5.11.11`|
|22|`@react-navigation/native`| `^5.9.4`|
|23|`@react-navigation/stack`| `^5.14.5`|
|24|`axios`| `^0.21.1`|
|25|`formik`| `^2.2.9`|
|26|`react`| `17.0.1`|


#### Environment
- Path Develop: `https://agatha-develop.herokuapp.com/api/`
- Path Production: `http://dev.najoconsultores.com:20044/api/`